package com.lgthouse.goldendoodle.Panel.Child.add;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.AppCompatSpinner;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;

import com.lgthouse.goldendoodle.POJO.Child;
import com.lgthouse.goldendoodle.POJO.Section;
import com.lgthouse.goldendoodle.Panel.PanelActivity;
import com.lgthouse.goldendoodle.R;

import java.util.List;

public class AddChildActivity extends Activity implements AddChildMvp.View {

    private EditText firstNameInput, lastNameInput, socialSecurityNumberInput;
    private Button confirmButton;
    private AppCompatSpinner sectionSpinner;
    private AddChildMvp.Presenter presenter;
    private ProgressDialog progressDialog;
    private ArrayAdapter<Section> sectionAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_child);

        this.presenter = new AddChildPresenter(this);
        this.sectionAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item);
        this.sectionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        this.firstNameInput = (EditText)findViewById(R.id.add_child_first_name_editText);
        this.lastNameInput = (EditText)findViewById(R.id.add_child_last_name_editText);
        this.socialSecurityNumberInput = (EditText)findViewById(R.id.add_child_social_security_number_editText);
        this.sectionSpinner = (AppCompatSpinner)findViewById(R.id.add_child_section_spinner);
        this.confirmButton = (Button)findViewById(R.id.add_child_confirm_button);

        this.sectionSpinner.setAdapter(sectionAdapter);
        this.confirmButton.setOnClickListener(view -> this.presenter.postChild());

        this.presenter.loadSections();
    }

    @Override
    public void setSections(List<Section> sections) {
        this.sectionAdapter.addAll(sections);
        this.sectionAdapter.notifyDataSetChanged();
    }

    @Override
    public void showPostProgress() {
        this.progressDialog = ProgressDialog.show(this, "", getString(R.string.postPDText), false);
    }

    @Override
    public void hideProgress() {
        this.progressDialog.dismiss();
    }

    @Override
    public void showPostConnectionFailed() {
        Snackbar snackbar = Snackbar.make(this.findViewById(R.id.activity_add_child), getString(R.string.connectionFailed), Snackbar.LENGTH_LONG)
                .setAction(getString(R.string.retryConnection), view -> this.presenter.postChild());
        snackbar.show();
    }

    @Override
    public void navigateToPanel() {
        this.startActivity(new Intent(this, PanelActivity.class));
    }

    @Override
    public String getFirstName() {
        return this.firstNameInput.getText().toString();
    }

    @Override
    public String getLastName() {
        return this.lastNameInput.getText().toString();
    }

    @Override
    public String getSocialSecurityNumber() {
        return this.socialSecurityNumberInput.getText().toString();
    }

    @Override
    public Section getSection() {
        return (Section)this.sectionSpinner.getSelectedItem();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.presenter.onDestroy();
    }
}
