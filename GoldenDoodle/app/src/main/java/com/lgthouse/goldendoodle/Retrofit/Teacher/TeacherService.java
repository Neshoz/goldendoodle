package com.lgthouse.goldendoodle.Retrofit.Teacher;

import com.lgthouse.goldendoodle.POJO.Teacher;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Nesho on 2017-05-05.
 */

public interface TeacherService {
    @GET("teachers")
    Call<List<Teacher>> getTeachers();

    @GET("teachers/{socialSecurityNumber}")
    Call<Teacher> getTeacher(@Path("socialSecurityNumber") String socialSecurityNumber);

    @POST("teachers")
    Call<Teacher> postTeacher(@Body Teacher teacher);

    @POST("teachers/update")
    Call<Teacher> updateTeacher(@Body Teacher teacher);

    @POST("teachers/{socialSecurityNumber}/delete")
    Call<Teacher> deleteTeacher(@Path("socialSecurityNumber") String socialSecurityNumber, @Body Teacher teacher);
}
