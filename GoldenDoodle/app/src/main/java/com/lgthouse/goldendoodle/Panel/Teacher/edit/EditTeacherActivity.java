package com.lgthouse.goldendoodle.Panel.Teacher.edit;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.lgthouse.goldendoodle.R;

public class EditTeacherActivity extends Activity {

    private static final String INTENT_EXTRA_SSN_STRING = "SSN";

    public static final Intent newIntent(Context caller, String socialSecurityNumber) {
        Intent intent = new Intent(caller, EditTeacherActivity.class);
        intent.putExtra(INTENT_EXTRA_SSN_STRING, socialSecurityNumber);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_teacher);
    }
}
