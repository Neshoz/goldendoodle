package com.lgthouse.goldendoodle.Panel.Teacher;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.lgthouse.goldendoodle.POJO.Teacher;
import com.lgthouse.goldendoodle.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nesho on 2017-05-08.
 */

public class TeacherAdapter extends RecyclerView.Adapter<TeacherAdapter.ViewHolder> {

    private Context context;
    private List<Teacher> teachers;
    private OnDeleteClickListener deleteClickListener;
    private OnEditClickListener editClickListener;

    public TeacherAdapter(Context context, OnDeleteClickListener deleteClickListener, OnEditClickListener editClickListener) {
        this.context = context;
        this.teachers = new ArrayList<>();
        this.deleteClickListener = deleteClickListener;
        this.editClickListener = editClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Teacher teacher = this.teachers.get(position);

        holder.itemView.setTag(teacher);
        holder.editIcon.setTag(teacher);
        holder.deleteIcon.setTag(teacher);

        holder.firstNameTextView.setText(teacher.getFirstName());
        holder.lastNameTextView.setText(teacher.getLastName());
        holder.ssnTextView.setText(teacher.getSocialSecurityNumber());

        holder.deleteIcon.setOnClickListener(view -> {
            Teacher clickedTeacher = (Teacher)view.getTag();
            deleteClickListener.onDeleteClicked(clickedTeacher);
        });

        holder.editIcon.setOnClickListener(view -> {
            Teacher clickedTeacher = (Teacher)view.getTag();
            editClickListener.onEditClicked(clickedTeacher);
        });
    }

    @Override
    public int getItemCount() {
        return this.teachers.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView firstNameTextView;
        private TextView lastNameTextView;
        private TextView ssnTextView;
        private ImageView deleteIcon;
        private ImageView editIcon;

        public ViewHolder(View itemView) {
            super(itemView);
            this.firstNameTextView = (TextView)itemView.findViewById(R.id.item_first_name_textView);
            this.lastNameTextView = (TextView)itemView.findViewById(R.id.item_last_name_textView);
            this.ssnTextView = (TextView)itemView.findViewById(R.id.item_ssn_textView);
            this.deleteIcon = (ImageView)itemView.findViewById(R.id.item_delete_icon);
            this.editIcon = (ImageView)itemView.findViewById(R.id.item_edit_icon);
        }
    }

    public interface OnDeleteClickListener {
        void onDeleteClicked(Teacher teacher);
    }

    public interface OnEditClickListener {
        void onEditClicked(Teacher teacher);
    }

    public void setItems(List<Teacher> items) {
        this.teachers.clear();
        this.teachers.addAll(items);
        this.notifyDataSetChanged();
    }

    public void removeItem(Teacher teacher)  {
        this.teachers.remove(teacher);
        this.notifyDataSetChanged();
    }
}
