package com.lgthouse.goldendoodle.Panel.Adult.edit;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.widget.Button;
import android.widget.EditText;

import com.lgthouse.goldendoodle.POJO.Adult;
import com.lgthouse.goldendoodle.Panel.PanelActivity;
import com.lgthouse.goldendoodle.R;

public class EditAdultActivity extends Activity implements EditAdultMvp.View {

    private static final String INTENT_EXTRA_STRING_SSN = "SSN";

    public static final Intent newIntent(Context caller, String socialSecurityNumber) {
        Intent intent = new Intent(caller, EditAdultActivity.class);
        intent.putExtra(INTENT_EXTRA_STRING_SSN, socialSecurityNumber);
        return intent;
    }

    private Adult downloadedAdult;
    private ProgressDialog progressDialog;
    private EditAdultMvp.Presenter presenter;
    private EditText firstNameInput, lastNameInput, phoneNumberInput, workPhoneNumberInput, emailInput;
    private Button submitButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_adult);

        this.firstNameInput = (EditText)findViewById(R.id.edit_adult_first_name_editText);
        this.lastNameInput = (EditText)findViewById(R.id.edit_adult_last_name_editText);
        this.phoneNumberInput = (EditText)findViewById(R.id.edit_adult_phone_number_editText);
        this.workPhoneNumberInput = (EditText)findViewById(R.id.edit_adult_work_number_editText);
        this.emailInput = (EditText)findViewById(R.id.edit_adult_email_editText);
        this.submitButton = (Button)findViewById(R.id.edit_adult_confirm_button);

        this.presenter = new EditAdultPresenter(this);
        this.presenter.loadAdult();

        this.submitButton.setOnClickListener(view -> this.presenter.postChanges());
    }

    @Override
    public void setAdult(Adult adult) {
        this.downloadedAdult = adult;
        this.firstNameInput.setText(adult.getFirstName());
        this.lastNameInput.setText(adult.getLastName());
        this.phoneNumberInput.setText(adult.getPhoneNumber());
        this.workPhoneNumberInput.setText(adult.getWorkPhoneNumber());
        this.emailInput.setText(adult.getEmail());
    }

    @Override
    public void showLoadProgress() {
        this.progressDialog = ProgressDialog.show(this, "", getString(R.string.downloadPDText), false);
    }

    @Override
    public void showPostProgress() {
        this.progressDialog = ProgressDialog.show(this, "", getString(R.string.postPDText), false);
    }

    @Override
    public void hideProgress() {
        this.progressDialog.dismiss();
    }

    @Override
    public void showDownloadConnectionFailed() {
        Snackbar snackbar = Snackbar.make(this.findViewById(R.id.activity_edit_adult), getString(R.string.connectionFailed), Snackbar.LENGTH_LONG)
                .setAction(getString(R.string.retryConnection), view -> this.presenter.loadAdult());
        snackbar.show();
    }

    @Override
    public void showPostConnectionFailed() {
        Snackbar snackbar = Snackbar.make(this.findViewById(R.id.activity_edit_adult), getString(R.string.connectionFailed), Snackbar.LENGTH_LONG)
                .setAction(getString(R.string.retryConnection), view -> this.presenter.postChanges());
        snackbar.show();
    }

    @Override
    public void navigateToPanel() {
        this.startActivity(new Intent(this, PanelActivity.class));
    }

    @Override
    public String getAdultSSN() {
        return this.getIntent().getStringExtra(INTENT_EXTRA_STRING_SSN);
    }

    @Override
    public String getFirstName() {
        return this.firstNameInput.getText().toString();
    }

    @Override
    public String getLastName() {
        return this.lastNameInput.getText().toString();
    }

    @Override
    public String getPhoneNumber() {
        return this.phoneNumberInput.getText().toString();
    }

    @Override
    public String getWorkPhoneNumber() {
        return this.workPhoneNumberInput.getText().toString();
    }

    @Override
    public String getEmail() {
        return this.emailInput.getText().toString();
    }

    @Override
    public Adult getAdult() {
        return this.downloadedAdult;
    }

    @Override
    public void onDestroy() {
        this.presenter.onDestroy();
        super.onDestroy();
    }
}
