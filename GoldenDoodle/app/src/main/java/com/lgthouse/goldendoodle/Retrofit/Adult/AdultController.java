package com.lgthouse.goldendoodle.Retrofit.Adult;

import com.lgthouse.goldendoodle.POJO.Adult;
import com.lgthouse.goldendoodle.Retrofit.Api;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Nesho on 2017-05-05.
 */

public class AdultController {

    private static final int RESPONSE_OK = 200;
    private static final int RESPONSE_OK_NO_CONTENT = 204;
    private AdultService service;

    public AdultController() {
        this.service = Api.getInstance().getAdultService();
    }

    public void getAllAdults(OnAdultsLoadedCallback callback) {
        Call<List<Adult>> call = this.service.getAllAdults();
        call.enqueue(new Callback<List<Adult>>() {
            @Override
            public void onResponse(Call<List<Adult>> call, Response<List<Adult>> response) {
                List<Adult> adults = response.body();
                callback.onAdultsLoaded(adults);
            }

            @Override
            public void onFailure(Call<List<Adult>> call, Throwable t) {
                t.printStackTrace();
                callback.onAdultsLoadedFailure();
            }
        });
    }

    public void deleteAdult(String socialSecurityNumber, Adult adult, OnDeleteAdultCallback callback) {
        Call<Adult> call = this.service.deleteAdult(socialSecurityNumber, adult);
        call.enqueue(new Callback<Adult>() {
            @Override
            public void onResponse(Call<Adult> call, Response<Adult> response) {
                if(call.isExecuted() && response.code() == RESPONSE_OK_NO_CONTENT) {
                    callback.onDeleteSuccess();
                }
            }

            @Override
            public void onFailure(Call<Adult> call, Throwable t) {
                t.printStackTrace();
                callback.onDeleteFailure();
            }
        });
    }

    public void loadAdult(String socialSecurityNumber, OnAdultLoadedCallback callback) {
        Call<Adult> call = this.service.getAdult(socialSecurityNumber);
        call.enqueue(new Callback<Adult>() {
            @Override
            public void onResponse(Call<Adult> call, Response<Adult> response) {
                Adult adult = response.body();
                callback.onAdultLoaded(adult);
            }

            @Override
            public void onFailure(Call<Adult> call, Throwable t) {
                t.printStackTrace();
                callback.onAdultLoadedFailure();
            }
        });
    }

    public void updateAdult(String socialSecurityNumber, Adult adult, OnAdultUpdateCallback callback) {
        Call<Adult> call = this.service.updateAdult(socialSecurityNumber, adult);
        call.enqueue(new Callback<Adult>() {
            @Override
            public void onResponse(Call<Adult> call, Response<Adult> response) {
                if(call.isExecuted() && response.code() == RESPONSE_OK_NO_CONTENT) {
                    callback.onUpdateSuccess();
                }
            }

            @Override
            public void onFailure(Call<Adult> call, Throwable t) {
                t.printStackTrace();
                callback.onUpdateFailure();
            }
        });
    }

    public void postAdult(String childSSN, Adult adult, OnPostAdultCallback callback) {
        Call<Adult> call = this.service.postAdult(childSSN, adult);
        call.enqueue(new Callback<Adult>() {
            @Override
            public void onResponse(Call<Adult> call, Response<Adult> response) {
                if(call.isExecuted() && response.code() == RESPONSE_OK_NO_CONTENT) {
                    callback.onPostSuccess();
                }
            }

            @Override
            public void onFailure(Call<Adult> call, Throwable t) {
                t.printStackTrace();
                callback.onPostFailure();
            }
        });
    }

    public interface OnAdultsLoadedCallback {
        void onAdultsLoaded(List<Adult> adults);
        void onAdultsLoadedFailure();
    }

    public interface OnAdultLoadedCallback {
        void onAdultLoaded(Adult adult);
        void onAdultLoadedFailure();
    }

    public interface OnPostAdultCallback {
        void onPostSuccess();
        void onPostFailure();
    }

    public interface OnAdultUpdateCallback {
        void onUpdateSuccess();
        void onUpdateFailure();
    }

    public interface OnDeleteAdultCallback {
        void onDeleteSuccess();
        void onDeleteFailure();
    }
}
