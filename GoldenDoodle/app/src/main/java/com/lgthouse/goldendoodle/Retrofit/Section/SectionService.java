package com.lgthouse.goldendoodle.Retrofit.Section;

import com.lgthouse.goldendoodle.POJO.Section;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Nesho on 2017-05-05.
 */

public interface SectionService {

    @GET("sections")
    Call<List<Section>> getAllSections();

    @GET("sections/{sectionName}")
    Call<Section> getSection(@Path("sectionName") String sectionName);

    @POST("sections")
    Call<Section> postSection(@Body Section section);

    @POST("sections/delete")
    Call<Section> deleteSection(@Body Section section);

    @POST("sections/update")
    Call<Section> updateSection(@Body Section section);
}
