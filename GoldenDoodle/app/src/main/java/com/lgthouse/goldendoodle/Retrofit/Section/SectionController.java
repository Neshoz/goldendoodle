package com.lgthouse.goldendoodle.Retrofit.Section;

import com.lgthouse.goldendoodle.POJO.Section;
import com.lgthouse.goldendoodle.Retrofit.Api;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Nesho on 2017-05-05.
 */

public class SectionController {

    private static final int RESPONSE_OK = 200;
    private static final int RESPONSE_OK_NO_CONTENT = 204;
    private SectionService service;

    public SectionController() {
        this.service = Api.getInstance().getSectionService();
    }

    public void getAllSections(OnSectionsLoadedCallback callback) {
        Call<List<Section>> call = this.service.getAllSections();
        call.enqueue(new Callback<List<Section>>() {
            @Override
            public void onResponse(Call<List<Section>> call, Response<List<Section>> response) {
                List<Section> sections = response.body();
                callback.onSectionsLoaded(sections);
            }

            @Override
            public void onFailure(Call<List<Section>> call, Throwable t) {
                t.printStackTrace();
                callback.onSectionsLoadedFailure();
            }
        });
    }

    public void getSection(String sectionName, OnSectionLoadedCallback callback) {
        Call<Section> call = this.service.getSection(sectionName);
        call.enqueue(new Callback<Section>() {
            @Override
            public void onResponse(Call<Section> call, Response<Section> response) {
                Section section = response.body();
                callback.onSectionLoaded(section);
            }

            @Override
            public void onFailure(Call<Section> call, Throwable t) {
                t.printStackTrace();
                callback.onSectionLoadedFailure();
            }
        });
    }

    public void postSection(Section section, OnSectionPostedCallback callback) {
        Call<Section> call = this.service.postSection(section);
        call.enqueue(new Callback<Section>() {
            @Override
            public void onResponse(Call<Section> call, Response<Section> response) {
                if(call.isExecuted() && response.code() == RESPONSE_OK_NO_CONTENT) {
                    callback.onPostSuccess();
                }
            }

            @Override
            public void onFailure(Call<Section> call, Throwable t) {
                t.printStackTrace();
                callback.onPostFailure();
            }
        });
    }

    public void deleteSection(Section section, OnSectionDeletedCallback callback) {
        Call<Section> call = this.service.deleteSection(section);
        call.enqueue(new Callback<Section>() {
            @Override
            public void onResponse(Call<Section> call, Response<Section> response) {
                if(call.isExecuted() && response.code() == RESPONSE_OK_NO_CONTENT) {
                    callback.onDeleteSuccess();
                }
            }

            @Override
            public void onFailure(Call<Section> call, Throwable t) {
                t.printStackTrace();
                callback.onDeleteFailure();
            }
        });
    }

    public void updateSection(Section section, OnSectionUpdatedCallback callback) {
        Call<Section> call = this.service.updateSection(section);
        call.enqueue(new Callback<Section>() {
            @Override
            public void onResponse(Call<Section> call, Response<Section> response) {
                if(call.isExecuted() && response.code() == RESPONSE_OK_NO_CONTENT) {
                    callback.onUpdateSuccess();
                }
            }

            @Override
            public void onFailure(Call<Section> call, Throwable t) {
                t.printStackTrace();
                callback.onUpdateFailure();
            }
        });
    }

    public interface OnSectionsLoadedCallback {
        void onSectionsLoaded(List<Section> sections);
        void onSectionsLoadedFailure();
    }

    public interface OnSectionLoadedCallback {
        void onSectionLoaded(Section section);
        void onSectionLoadedFailure();
    }

    public interface OnSectionPostedCallback {
        void onPostSuccess();
        void onPostFailure();
    }

    public interface OnSectionDeletedCallback {
        void onDeleteSuccess();
        void onDeleteFailure();
    }

    public interface OnSectionUpdatedCallback {
        void onUpdateSuccess();
        void onUpdateFailure();
    }
}
