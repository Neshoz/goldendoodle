package com.lgthouse.goldendoodle.Panel.Child.add;

import com.lgthouse.goldendoodle.POJO.Child;
import com.lgthouse.goldendoodle.POJO.Section;
import com.lgthouse.goldendoodle.Retrofit.Child.ChildController;
import com.lgthouse.goldendoodle.Retrofit.Section.SectionController;

import java.util.List;

/**
 * Created by Nesho on 2017-05-09.
 */

public class AddChildPresenter implements AddChildMvp.Presenter {

    private AddChildMvp.View view;
    private ChildController childController;
    private SectionController sectionController;

    public AddChildPresenter(AddChildMvp.View view) {
        this.view = view;
        this.childController = new ChildController();
        this.sectionController = new SectionController();
    }

    @Override
    public void postChild() {
        Child child = new Child();
        child.setFirstName(this.view.getFirstName());
        child.setLastName(this.view.getLastName());
        child.setSocialSecurityNumber(this.view.getSocialSecurityNumber());

        this.view.showPostProgress();
        this.childController.postChild(this.view.getSection().getSectionName(), child, new ChildController.OnChildPostedCallback() {
            @Override
            public void onPostSuccess() {
                view.hideProgress();
                view.navigateToPanel();
            }

            @Override
            public void onPostFailure() {
                view.hideProgress();
                view.showPostConnectionFailed();
            }
        });
    }

    @Override
    public void loadSections() {
        this.sectionController.getAllSections(new SectionController.OnSectionsLoadedCallback() {
            @Override
            public void onSectionsLoaded(List<Section> sections) {
                view.setSections(sections);
            }

            @Override
            public void onSectionsLoadedFailure() {

            }
        });
    }

    @Override
    public void onDestroy() {
        this.view = null;
    }
}
