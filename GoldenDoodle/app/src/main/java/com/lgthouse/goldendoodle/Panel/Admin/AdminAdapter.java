package com.lgthouse.goldendoodle.Panel.Admin;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.lgthouse.goldendoodle.POJO.Admin;
import com.lgthouse.goldendoodle.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nesho on 2017-05-08.
 */

public class AdminAdapter extends RecyclerView.Adapter<AdminAdapter.ViewHolder> {

    private Context context;
    private List<Admin> admins;
    private OnDeleteClickedListener deleteClickedListener;
    private OnEditClickedListener editClickedListener;

    public AdminAdapter(Context context, OnDeleteClickedListener deleteClickedListener, OnEditClickedListener editClickedListener) {
        this.context = context;
        this.admins = new ArrayList<>();
        this.deleteClickedListener = deleteClickedListener;
        this.editClickedListener = editClickedListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Admin admin = this.admins.get(position);
        holder.itemView.setTag(admin);
        holder.deleteImageView.setTag(admin);
        holder.editImageView.setTag(admin);

        holder.firstNameTextView.setText(admin.getFirstName());
        holder.lastNameTextView.setText(admin.getLastName());
        holder.ssnTextView.setText("''" + admin.getUserName() + "''");

        holder.deleteImageView.setOnClickListener(view -> {
            Admin clickedAdmin = (Admin)view.getTag();
            deleteClickedListener.onDeleteClicked(clickedAdmin);
        });

        holder.editImageView.setOnClickListener(view -> {
            Admin clickedAdmin = (Admin)view.getTag();
            editClickedListener.onEditClicked(clickedAdmin);
        });
    }

    @Override
    public int getItemCount() {
        return this.admins.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView firstNameTextView;
        private TextView lastNameTextView;
        private TextView ssnTextView;
        private ImageView editImageView;
        private ImageView deleteImageView;

        public ViewHolder(View itemView) {
            super(itemView);
            this.firstNameTextView = (TextView)itemView.findViewById(R.id.item_first_name_textView);
            this.lastNameTextView = (TextView)itemView.findViewById(R.id.item_last_name_textView);
            this.ssnTextView = (TextView)itemView.findViewById(R.id.item_ssn_textView);
            this.editImageView = (ImageView)itemView.findViewById(R.id.item_edit_icon);
            this.deleteImageView = (ImageView)itemView.findViewById(R.id.item_delete_icon);
        }
    }

    public interface OnDeleteClickedListener {
        void onDeleteClicked(Admin admin);
    }

    public interface OnEditClickedListener {
        void onEditClicked(Admin admin);
    }

    public void setItems(List<Admin> items) {
        this.admins.clear();
        this.admins.addAll(items);
        this.notifyDataSetChanged();
    }

    public void removeItem(Admin admin) {
        this.admins.remove(admin);
        this.notifyDataSetChanged();
    }
}
