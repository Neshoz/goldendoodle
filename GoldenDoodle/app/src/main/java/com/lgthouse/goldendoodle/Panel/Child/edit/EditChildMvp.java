package com.lgthouse.goldendoodle.Panel.Child.edit;

import com.lgthouse.goldendoodle.POJO.Child;
import com.lgthouse.goldendoodle.POJO.Section;

import java.util.List;

/**
 * Created by Nesho on 2017-05-09.
 */

public interface EditChildMvp {

    interface View {
        void setChild(Child child);
        void setSections(List<Section> sections);
        void showDownloadProgress();
        void showPostProgress();
        void hideProgress();
        void showDownloadConnectionFailed();
        void showPostConnectionFailed();
        void navigateToPanel();
        void setSelectedSpinnerItem(List<Section> sections);
        String getFirstName();
        String getLastName();
        String getSocialSecurityNumber();
        Section getSection();
        Child getChild();
    }

    interface Presenter {
        void loadChild();
        void loadSections();
        void updateChild();
        void setSelectedSpinnerItem(List<Section> sections);
        void onDestroy();
    }
}
