package com.lgthouse.goldendoodle.Retrofit.Teacher;

import com.lgthouse.goldendoodle.POJO.Teacher;
import com.lgthouse.goldendoodle.Retrofit.Api;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Nesho on 2017-05-05.
 */

public class TeacherController {

    private static final int RESPONSE_OK = 200;
    private static final int RESPONSE_OK_NO_CONTENT = 204;
    private TeacherService service;

    public TeacherController() {
        this.service = Api.getInstance().getTeacherService();
    }

    public void getAllTeachers(OnTeachersLoadedCallback callback) {
        Call<List<Teacher>> call = this.service.getTeachers();
        call.enqueue(new Callback<List<Teacher>>() {
            @Override
            public void onResponse(Call<List<Teacher>> call, Response<List<Teacher>> response) {
                List<Teacher> teachers = response.body();
                callback.onTeachersLoaded(teachers);
            }

            @Override
            public void onFailure(Call<List<Teacher>> call, Throwable t) {
                t.printStackTrace();
                callback.onTeachersLoadedFailure();
            }
        });
    }

    public void getTeacher(String socialSecurityNumber, OnTeacherLoadedCallback callback) {
        Call<Teacher> call = this.service.getTeacher(socialSecurityNumber);
        call.enqueue(new Callback<Teacher>() {
            @Override
            public void onResponse(Call<Teacher> call, Response<Teacher> response) {
                Teacher teacher = response.body();
                callback.onTeacherLoaded(teacher);
            }

            @Override
            public void onFailure(Call<Teacher> call, Throwable t) {
                t.printStackTrace();
                callback.onTeacherLoadedFailure();
            }
        });
    }

    public void postTeacher(Teacher teacher, OnTeacherPostedCallback callback) {
        Call<Teacher> call = this.service.postTeacher(teacher);
        call.enqueue(new Callback<Teacher>() {
            @Override
            public void onResponse(Call<Teacher> call, Response<Teacher> response) {
                if(call.isExecuted() && response.code() == RESPONSE_OK_NO_CONTENT) {
                    callback.onPostSuccess();
                }
            }

            @Override
            public void onFailure(Call<Teacher> call, Throwable t) {
                t.printStackTrace();
                callback.onPostFailure();
            }
        });
    }

    public void updateTeacher(Teacher teacher, OnTeacherUpdatedCallback callback) {
        Call<Teacher> call = this.service.updateTeacher(teacher);
        call.enqueue(new Callback<Teacher>() {
            @Override
            public void onResponse(Call<Teacher> call, Response<Teacher> response) {
                if(call.isExecuted() && response.code() == RESPONSE_OK_NO_CONTENT) {
                    callback.onUpdateSuccess();
                }
            }

            @Override
            public void onFailure(Call<Teacher> call, Throwable t) {
                t.printStackTrace();
                callback.onUpdateFailure();
            }
        });
    }

    public void deleteTeacher(String socialSecurityNumber, Teacher teacher, OnTeacherDeletedCallback callback) {
        Call<Teacher> call = this.service.deleteTeacher(socialSecurityNumber, teacher);
        call.enqueue(new Callback<Teacher>() {
            @Override
            public void onResponse(Call<Teacher> call, Response<Teacher> response) {
                if(call.isExecuted() && response.code() == RESPONSE_OK_NO_CONTENT) {
                    callback.onDeleteSuccess();
                }
            }

            @Override
            public void onFailure(Call<Teacher> call, Throwable t) {
                t.printStackTrace();
                callback.onDeleteFailure();
            }
        });
    }

    public interface OnTeachersLoadedCallback {
        void onTeachersLoaded(List<Teacher> teachers);
        void onTeachersLoadedFailure();
    }

    public interface OnTeacherLoadedCallback {
        void onTeacherLoaded(Teacher teacher);
        void onTeacherLoadedFailure();
    }

    public interface OnTeacherPostedCallback {
        void onPostSuccess();
        void onPostFailure();
    }

    public interface OnTeacherDeletedCallback {
        void onDeleteSuccess();
        void onDeleteFailure();
    }

    public interface OnTeacherUpdatedCallback {
        void onUpdateSuccess();
        void onUpdateFailure();
    }
}
