package com.lgthouse.goldendoodle.Panel.Section.edit;

import com.lgthouse.goldendoodle.POJO.Section;

/**
 * Created by Nesho on 2017-05-09.
 */

public interface EditSectionMvp {

    interface View {
        void setSection(Section section);
        void showDownloadProgress();
        void showPostProgress();
        void hideProgress();
        void showDownloadConnectionFailed();
        void showPostConnectionFailed();
        void navigateToPanel();
        String getSectionName();
        String getPassword();
        Section getSection();
        String getIntentSectionName();
    }

    interface Presenter {
        void loadSection();
        void updateSection();
        void onDestroy();
    }
}
