package com.lgthouse.goldendoodle.Panel.Adult;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.lgthouse.goldendoodle.POJO.Adult;
import com.lgthouse.goldendoodle.Panel.Adult.add.AddAdultActivity;
import com.lgthouse.goldendoodle.Panel.Adult.edit.EditAdultActivity;
import com.lgthouse.goldendoodle.R;

import java.util.List;

/**
 * Created by Nesho on 2017-05-08.
 */

public class AdultFragment extends Fragment implements AdultMvp.View, AdultAdapter.OnEditClickedListener, AdultAdapter.OnDeleteClickListener {

    private AdultAdapter adultAdapter;
    private AdultMvp.Presenter presenter;
    private ProgressBar progressBar;
    private RecyclerView recyclerView;
    private FloatingActionButton addButton;
    private Adult clickedAdult;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_adult, container, false);

        this.adultAdapter = new AdultAdapter(this.getContext(), this, this);
        this.presenter = new AdultPresenter(this);

        this.progressBar = (ProgressBar)view.findViewById(R.id.adult_fragment_loading_spinner);
        this.recyclerView = (RecyclerView)view.findViewById(R.id.adult_list);
        this.addButton = (FloatingActionButton)view.findViewById(R.id.adult_add_button);
        this.addButton.setOnClickListener(btn -> this.presenter.navigateToAdd());

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this.getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adultAdapter);

        this.presenter.loadAdults();

        return view;
    }

    @Override
    public void setAdults(List<Adult> adults) {
        this.adultAdapter.setItems(adults);
    }

    @Override
    public void removeAdult(Adult adult) {
        this.adultAdapter.removeItem(adult);
    }

    @Override
    public void showProgress() {
        this.recyclerView.setVisibility(View.GONE);
        this.progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        this.progressBar.setVisibility(View.GONE);
        this.recyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showConnectionFailed() {
        Snackbar snackbar = Snackbar.make(this.getActivity().findViewById(R.id.activity_panel),
                getString(R.string.connectionFailed),
                Snackbar.LENGTH_LONG)
                .setAction(getString(R.string.retryConnection), view -> this.presenter.loadAdults());
        snackbar.show();
    }

    @Override
    public void showDeleteConnectionFailed() {
        Snackbar snackbar = Snackbar.make(this.getActivity().findViewById(R.id.activity_panel),
                getString(R.string.connectionFailed),
                Snackbar.LENGTH_LONG)
                .setAction(getString(R.string.retryConnection), view -> this.presenter.removeAdult(this.clickedAdult));
        snackbar.show();
    }

    @Override
    public void navigateToEdit(Adult adult) {
        this.startActivity(EditAdultActivity.newIntent(this.getContext(), adult.getSocialSecurityNumber()));
    }

    @Override
    public void navigateToAdd() {
        this.startActivity(new Intent(this.getContext(), AddAdultActivity.class));
    }

    @Override
    public void onDeleteClicked(Adult adult) {
        this.clickedAdult = adult;
        this.presenter.removeAdult(adult);
    }

    @Override
    public void onEditClicked(Adult adult) {
        this.presenter.navigateToEdit(adult);
    }

    @Override
    public void onDestroy() {
        this.presenter.onDestroy();
        super.onDestroy();
    }
}
