package com.lgthouse.goldendoodle.Retrofit.Admin;

import com.lgthouse.goldendoodle.POJO.Admin;
import com.lgthouse.goldendoodle.Retrofit.Api;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Nesho on 2017-05-05.
 */

public class AdminController {

    private static final int RESPONSE_OK = 200;
    public static final int RESPONSE_OK_NO_CONTENT = 204;
    private AdminService service;

    public AdminController() {
        this.service = Api.getInstance().getAdminService();
    }

    public void getAllAdmins(OnAdminsLoadedCallback callback) {
        Call<List<Admin>> call = this.service.getAllAdmins();
        call.enqueue(new Callback<List<Admin>>() {
            @Override
            public void onResponse(Call<List<Admin>> call, Response<List<Admin>> response) {
                List<Admin> admins = response.body();
                callback.onAdminsLoaded(admins);
            }

            @Override
            public void onFailure(Call<List<Admin>> call, Throwable t) {
                t.printStackTrace();
                callback.onAdminsLoadedFailure();
            }
        });
    }

    public void getAdmin(String userName, final OnAdminLoadedCallback callback) {
        Call<Admin> call = this.service.getAdmin(userName);

        call.enqueue(new Callback<Admin>() {
            @Override
            public void onResponse(Call<Admin> call, Response<Admin> response) {
                Admin admin = response.body();
                callback.onAdminLoaded(admin);
            }

            @Override
            public void onFailure(Call<Admin> call, Throwable t) {
                t.printStackTrace();
                callback.onAdminLoadedFailure();
            }
        });
    }

    public void postAdmin(Admin admin, final OnAdminPostedCallback callback) {
        Call<Admin> call = this.service.postAdmin(admin);

        call.enqueue(new Callback<Admin>() {
            @Override
            public void onResponse(Call<Admin> call, Response<Admin> response) {
                if(call.isExecuted() && response.code() == RESPONSE_OK_NO_CONTENT) {
                    callback.onPostSuccess();
                }
            }

            @Override
            public void onFailure(Call<Admin> call, Throwable t) {
                callback.onPostFailure();
            }
        });
    }

    public void updateAdmin(Admin admin, OnAdminUpdatedCallback callback) {
        Call<Admin> call = this.service.updateAdmin(admin);
        call.enqueue(new Callback<Admin>() {
            @Override
            public void onResponse(Call<Admin> call, Response<Admin> response) {
                if(call.isExecuted() && response.code() == RESPONSE_OK_NO_CONTENT) {
                    callback.onUpdateSuccess();
                }
            }

            @Override
            public void onFailure(Call<Admin> call, Throwable t) {
                t.printStackTrace();
                callback.onUpdateFailure();
            }
        });
    }

    public void deleteAdmin(Admin admin, OnAdminDeletedCallback callback) {
        Call<Admin> call = this.service.deleteAdmin(admin);
        call.enqueue(new Callback<Admin>() {
            @Override
            public void onResponse(Call<Admin> call, Response<Admin> response) {
                if(call.isExecuted() && response.code() == RESPONSE_OK_NO_CONTENT) {
                    callback.onDeleteSuccess();
                }
            }

            @Override
            public void onFailure(Call<Admin> call, Throwable t) {
                t.printStackTrace();
                callback.onDeleteFailure();
            }
        });
    }

    public interface OnAdminsLoadedCallback {
        void onAdminsLoaded(List<Admin> admins);
        void onAdminsLoadedFailure();
    }

    public interface OnAdminLoadedCallback {
        void onAdminLoaded(Admin admin);
        void onAdminLoadedFailure();
    }

    public interface OnAdminPostedCallback {
        void onPostSuccess();
        void onPostFailure();
    }

    public interface OnAdminUpdatedCallback {
        void onUpdateSuccess();
        void onUpdateFailure();
    }

    public interface OnAdminDeletedCallback {
        void onDeleteSuccess();
        void onDeleteFailure();
    }
}
