package com.lgthouse.goldendoodle.Panel.Section;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.lgthouse.goldendoodle.POJO.Section;
import com.lgthouse.goldendoodle.Panel.Section.edit.EditSectionActivity;
import com.lgthouse.goldendoodle.R;

import java.util.List;

/**
 * Created by Nesho on 2017-05-08.
 */

public class SectionFragment extends Fragment implements SectionMvp.View, SectionAdapter.OnEditClickListener {

    private SectionMvp.Presenter presenter;
    private RecyclerView recyclerView;
    private SectionAdapter sectionAdapter;
    private ProgressBar progressBar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_section, container, false);

        this.presenter = new SectionPresenter(this);
        this.sectionAdapter = new SectionAdapter(this.getContext(), this);

        this.recyclerView = (RecyclerView)view.findViewById(R.id.section_list);
        this.progressBar = (ProgressBar)view.findViewById(R.id.section_fragment_loading_spinner);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this.getContext());
        this.recyclerView.setLayoutManager(layoutManager);
        this.recyclerView.setAdapter(sectionAdapter);

        this.presenter.loadSections();

        return view;
    }

    @Override
    public void setSections(List<Section> sections) {
        this.sectionAdapter.setItems(sections);
    }

    @Override
    public void showProgress() {
        this.recyclerView.setVisibility(View.GONE);
        this.progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        this.progressBar.setVisibility(View.GONE);
        this.recyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showConnectionFailed() {
        Snackbar snackbar = Snackbar.make(this.getActivity().findViewById(R.id.activity_panel), getString(R.string.connectionFailed), Snackbar.LENGTH_LONG)
                .setAction(getString(R.string.retryConnection), view -> this.presenter.loadSections());
        snackbar.show();
    }

    @Override
    public void navigateToEdit(Section section) {
        this.startActivity(EditSectionActivity.newIntent(this.getContext(), section.getSectionName()));
    }

    @Override
    public void onEditClicked(Section section) {
        this.presenter.navigateToEdit(section);
    }

    @Override
    public void onDestroy() {
        this.presenter.onDestroy();
        super.onDestroy();
    }
}
