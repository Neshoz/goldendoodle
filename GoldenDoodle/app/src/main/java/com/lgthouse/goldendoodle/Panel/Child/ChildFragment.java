package com.lgthouse.goldendoodle.Panel.Child;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.lgthouse.goldendoodle.POJO.Child;
import com.lgthouse.goldendoodle.Panel.Child.add.AddChildActivity;
import com.lgthouse.goldendoodle.Panel.Child.edit.EditChildActivity;
import com.lgthouse.goldendoodle.R;

import java.util.List;

/**
 * Created by Nesho on 2017-05-08.
 */

public class ChildFragment extends Fragment implements ChildMvp.View, ChildAdapter.OnDeleteClickListener, ChildAdapter.OnEditClickListener {

    private FloatingActionButton addButton;
    private ProgressBar progressBar;
    private ChildMvp.Presenter presenter;
    private ChildAdapter childAdapter;
    private RecyclerView recyclerView;
    private Child clickedChild;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_child, container, false);
        this.presenter = new ChildPresenter(this);
        this.childAdapter = new ChildAdapter(this.getContext(), this, this);

        this.recyclerView = (RecyclerView)view.findViewById(R.id.child_list);
        this.progressBar = (ProgressBar)view.findViewById(R.id.child_fragment_loading_spinner);
        this.addButton = (FloatingActionButton)view.findViewById(R.id.child_add_button);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this.getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(childAdapter);

        this.addButton.setOnClickListener(btn -> this.presenter.navigateToAdd());
        this.presenter.loadChildren();

        return view;
    }

    @Override
    public void setChildren(List<Child> children) {
        this.childAdapter.setItems(children);
    }

    @Override
    public void showProgress() {
        this.recyclerView.setVisibility(View.GONE);
        this.progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        this.progressBar.setVisibility(View.GONE);
        this.recyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showConnectionFailed() {
        Snackbar snackbar = Snackbar.make(this.getActivity().findViewById(R.id.activity_panel), getString(R.string.connectionFailed), Snackbar.LENGTH_LONG)
                .setAction(getString(R.string.retryConnection), view -> this.presenter.loadChildren());
        snackbar.show();
    }

    @Override
    public void showDeleteConnectionFailed() {
        Snackbar snackbar = Snackbar.make(this.getActivity().findViewById(R.id.activity_panel), getString(R.string.connectionFailed), Snackbar.LENGTH_LONG)
                .setAction(getString(R.string.retryConnection), view -> this.presenter.deleteChild(this.clickedChild));
        snackbar.show();
    }

    @Override
    public void navigateToAdd() {
        this.startActivity(new Intent(this.getContext(), AddChildActivity.class));
    }

    @Override
    public void navigateToEdit(Child child) {
        this.startActivity(EditChildActivity.newIntent(this.getContext(), child.getSocialSecurityNumber()));
    }

    @Override
    public void removeChild(Child child) {
        this.clickedChild = child;
        this.childAdapter.removeItem(child);
    }

    @Override
    public void onDeleteClicked(Child child) {
        this.presenter.deleteChild(child);
    }

    @Override
    public void onEditClicked(Child child) {
        this.presenter.navigateToEdit(child);
    }
    @Override
    public void onDestroy() {
        this.presenter.onDestroy();
        super.onDestroy();
    }
}
