package com.lgthouse.goldendoodle.Panel.Adult.add;

import com.lgthouse.goldendoodle.POJO.Adult;
import com.lgthouse.goldendoodle.Retrofit.Adult.AdultController;

/**
 * Created by Nesho on 2017-05-09.
 */

public class AddAdultPresenter implements AddAdultMvp.Presenter {

    private AddAdultMvp.View view;
    private AdultController adultController;

    public AddAdultPresenter(AddAdultMvp.View view) {
        this.view = view;
        this.adultController = new AdultController();
    }

    @Override
    public void postAdult() {
        Adult adult = new Adult();

        adult.setFirstName(this.view.getFirstName());
        adult.setLastName(this.view.getLastName());
        adult.setSocialSecurityNumber(this.view.getSocialSecurityNumber());
        adult.setPhoneNumber(this.view.getPhoneNumber());
        adult.setWorkPhoneNumber(this.view.getWorkPhoneNumber());
        adult.setEmail(this.view.getEmail());
        adult.setRole(this.view.getRole());

        this.view.showPostProgress();
        this.adultController.postAdult(this.view.getChildSocialSecurityNumber(), adult, new AdultController.OnPostAdultCallback() {
            @Override
            public void onPostSuccess() {
                view.hideProgress();
                view.navigateToPanel();
            }

            @Override
            public void onPostFailure() {
                view.hideProgress();
                view.showConnectionFailed();
            }
        });
    }

    @Override
    public void onDestroy() {
        this.view = null;
    }
}
