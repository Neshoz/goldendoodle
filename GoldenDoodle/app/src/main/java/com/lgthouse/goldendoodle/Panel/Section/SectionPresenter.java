package com.lgthouse.goldendoodle.Panel.Section;

import com.lgthouse.goldendoodle.POJO.Section;
import com.lgthouse.goldendoodle.Retrofit.Section.SectionController;

import java.util.List;

/**
 * Created by Nesho on 2017-05-08.
 */

public class SectionPresenter implements SectionMvp.Presenter {

    private SectionMvp.View view;
    private SectionController sectionController;

    public SectionPresenter(SectionMvp.View view)  {
        this.view = view;
        this.sectionController = new SectionController();
    }
    @Override
    public void loadSections() {
        this.view.showProgress();
        this.sectionController.getAllSections(new SectionController.OnSectionsLoadedCallback() {
            @Override
            public void onSectionsLoaded(List<Section> sections) {
                view.hideProgress();
                view.setSections(sections);
            }

            @Override
            public void onSectionsLoadedFailure() {
                view.hideProgress();
                view.showConnectionFailed();
            }
        });
    }

    @Override
    public void navigateToEdit(Section section) {
        this.view.navigateToEdit(section);
    }

    @Override
    public void onDestroy() {
        this.view = null;
    }
}
