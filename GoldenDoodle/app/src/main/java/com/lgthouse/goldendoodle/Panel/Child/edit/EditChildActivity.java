package com.lgthouse.goldendoodle.Panel.Child.edit;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.AppCompatSpinner;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;

import com.lgthouse.goldendoodle.POJO.Child;
import com.lgthouse.goldendoodle.POJO.Section;
import com.lgthouse.goldendoodle.Panel.PanelActivity;
import com.lgthouse.goldendoodle.R;

import java.util.List;

public class EditChildActivity extends Activity implements EditChildMvp.View {

    private static final String INTENT_STRING_EXTRA_SSN = "SSN";

    public static final Intent newIntent(Context caller, String ssn) {
        Intent intent = new Intent(caller, EditChildActivity.class);
        intent.putExtra(INTENT_STRING_EXTRA_SSN, ssn);
        return intent;
    }

    private EditText firstNameInput, lastNameInput;
    private AppCompatSpinner sectionSpinner;
    private Button confirmButton;
    private ProgressDialog progressDialog;
    private EditChildMvp.Presenter presenter;
    private ArrayAdapter<Section> sectionAdapter;
    private Child downloadedChild;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_child);

        this.presenter = new EditChildPresenter(this);
        this.sectionAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item);
        this.sectionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        this.firstNameInput = (EditText)findViewById(R.id.edit_child_first_name_editText);
        this.lastNameInput = (EditText)findViewById(R.id.edit_child_last_name_editText);
        this.sectionSpinner = (AppCompatSpinner)findViewById(R.id.edit_child_section_spinner);
        this.confirmButton = (Button)findViewById(R.id.edit_child_confirm_button);

        this.sectionSpinner.setAdapter(sectionAdapter);
        this.confirmButton.setOnClickListener(view -> this.presenter.updateChild());

        this.presenter.loadChild();
        this.presenter.loadSections();
    }

    @Override
    public void setChild(Child child) {
        this.downloadedChild = child;
        this.firstNameInput.setText(child.getFirstName());
        this.lastNameInput.setText(child.getLastName());
    }

    @Override
    public void setSections(List<Section> sections) {
        this.sectionAdapter.clear();
        this.sectionAdapter.addAll(sections);
        this.sectionAdapter.notifyDataSetChanged();
        this.presenter.setSelectedSpinnerItem(sections);
    }

    @Override
    public void showDownloadProgress() {
        this.progressDialog = ProgressDialog.show(this, "", getString(R.string.downloadPDText), false);
    }

    @Override
    public void showPostProgress() {
        this.progressDialog = ProgressDialog.show(this, "", getString(R.string.postPDText), false);
    }

    @Override
    public void hideProgress() {
        this.progressDialog.dismiss();
    }

    @Override
    public void showDownloadConnectionFailed() {
        Snackbar snackbar = Snackbar.make(this.findViewById(R.id.activity_edit_child), getString(R.string.connectionFailed), Snackbar.LENGTH_LONG)
                .setAction(getString(R.string.retryConnection), view -> this.presenter.loadChild());
        snackbar.show();
    }

    @Override
    public void showPostConnectionFailed() {
        Snackbar snackbar = Snackbar.make(this.findViewById(R.id.activity_edit_child), getString(R.string.connectionFailed), Snackbar.LENGTH_LONG)
                .setAction(getString(R.string.retryConnection), view -> this.presenter.updateChild());
        snackbar.show();
    }

    @Override
    public void navigateToPanel() {
        this.startActivity(new Intent(this, PanelActivity.class));
    }

    @Override
    public void setSelectedSpinnerItem(List<Section> sections) {
        int foundSectionIndex = 0;
        if(this.downloadedChild != null) {
            for(int i = 0; i < sections.size(); i++) {
                if(sections.get(i).getSectionId() == this.downloadedChild.getSectionId()) {
                    foundSectionIndex = i;
                    break;
                }
            }
        }
        this.sectionSpinner.setSelection(foundSectionIndex);
    }

    @Override
    public String getFirstName() {
        return this.firstNameInput.getText().toString();
    }

    @Override
    public String getLastName() {
        return this.lastNameInput.getText().toString();
    }

    @Override
    public String getSocialSecurityNumber() {
        return this.getIntent().getStringExtra(INTENT_STRING_EXTRA_SSN);
    }

    @Override
    public Section getSection() {
        return (Section)this.sectionSpinner.getSelectedItem();
    }

    @Override
    public Child getChild() {
        return this.downloadedChild;
    }

    @Override
    public void onDestroy() {
        this.presenter.onDestroy();
        super.onDestroy();
    }
}
