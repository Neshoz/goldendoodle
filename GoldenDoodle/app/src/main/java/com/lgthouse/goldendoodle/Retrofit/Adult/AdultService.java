package com.lgthouse.goldendoodle.Retrofit.Adult;

import com.lgthouse.goldendoodle.POJO.Adult;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Nesho on 2017-05-05.
 */

public interface AdultService {
    @GET("adults")
    Call<List<Adult>> getAllAdults();

    @GET("adults/{socialSecurityNumber}")
    Call<Adult> getAdult(@Path("socialSecurityNumber") String socialSecurityNumber);

    @POST("adults/{childSSN}")
    Call<Adult> postAdult(@Path("childSSN") String childSSN, @Body Adult adult);

    @POST("adults/{socialSecurityNumber}/updateAdult")
    Call<Adult> updateAdult(@Path("socialSecurityNumber") String socialSecurityNumber, @Body Adult adult);

    @POST("adults/{socialSecurityNumber}/delete")
    Call<Adult> deleteAdult(@Path("socialSecurityNumber") String socialSecurityNumber, @Body Adult adult);
}
