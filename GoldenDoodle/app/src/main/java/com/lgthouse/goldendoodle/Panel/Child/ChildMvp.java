package com.lgthouse.goldendoodle.Panel.Child;

import com.lgthouse.goldendoodle.POJO.Child;

import java.util.List;

/**
 * Created by Nesho on 2017-05-08.
 */

public interface ChildMvp {

    interface View {
        void setChildren(List<Child> children);
        void showProgress();
        void hideProgress();
        void showConnectionFailed();
        void showDeleteConnectionFailed();
        void navigateToAdd();
        void navigateToEdit(Child child);
        void removeChild(Child child);
    }

    interface Presenter {
        void loadChildren();
        void deleteChild(Child child);
        void navigateToAdd();
        void navigateToEdit(Child child);
        void onDestroy();
    }
}
