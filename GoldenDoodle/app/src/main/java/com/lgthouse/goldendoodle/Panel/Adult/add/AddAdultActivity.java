package com.lgthouse.goldendoodle.Panel.Adult.add;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.AppCompatSpinner;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;

import com.lgthouse.goldendoodle.POJO.Adult;
import com.lgthouse.goldendoodle.Panel.PanelActivity;
import com.lgthouse.goldendoodle.R;

public class AddAdultActivity extends Activity implements AddAdultMvp.View {

    private AddAdultMvp.Presenter presenter;
    private ProgressDialog progressDialog;
    private EditText firstNameInput, lastNameInput, socialSecurityNumberInput, phoneNumberInput, workPhoneNumberInput, emailInput, childSocialSecurityNumber;
    private AppCompatSpinner roleSpinner;
    private Button submitButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_adult);
        this.presenter = new AddAdultPresenter(this);

        this.firstNameInput = (EditText)findViewById(R.id.add_adult_first_name_editText);
        this.lastNameInput = (EditText)findViewById(R.id.add_adult_last_name_editText);
        this.socialSecurityNumberInput = (EditText)findViewById(R.id.add_adult_social_security_number_editText);
        this.phoneNumberInput = (EditText)findViewById(R.id.add_adult_phone_number_editText);
        this.workPhoneNumberInput = (EditText)findViewById(R.id.add_adult_work_number_editText);
        this.emailInput = (EditText)findViewById(R.id.add_adult_email_editText);
        this.childSocialSecurityNumber = (EditText)findViewById(R.id.add_adult_child_ssn_editText);
        this.roleSpinner = (AppCompatSpinner)findViewById(R.id.add_adult_role_spinner);
        this.submitButton = (Button)findViewById(R.id.add_adult_confirm_button);

        this.submitButton.setOnClickListener(view -> this.presenter.postAdult());

        String[] roles = this.getResources().getStringArray(R.array.childPickUpRoles);
        ArrayAdapter<String> roleAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, roles);
        roleAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        roleSpinner.setAdapter(roleAdapter);
    }

    @Override
    public void showPostProgress() {
        this.progressDialog = ProgressDialog.show(this, "", getString(R.string.postPDText), false);
    }

    @Override
    public void hideProgress() {
        this.progressDialog.dismiss();
    }

    @Override
    public void showConnectionFailed() {
        Snackbar snackbar = Snackbar.make(this.findViewById(R.id.activity_add_adult), getString(R.string.connectionFailed), Snackbar.LENGTH_LONG)
                .setAction(getString(R.string.retryConnection), view -> this.presenter.postAdult());
        snackbar.show();
    }

    @Override
    public void navigateToPanel() {
        this.startActivity(new Intent(this, PanelActivity.class));
    }

    @Override
    public String getFirstName() {
        return this.firstNameInput.getText().toString();
    }

    @Override
    public String getLastName() {
        return this.lastNameInput.getText().toString();
    }

    @Override
    public String getSocialSecurityNumber() {
        return this.socialSecurityNumberInput.getText().toString();
    }

    @Override
    public String getPhoneNumber() {
        return this.phoneNumberInput.getText().toString();
    }

    @Override
    public String getWorkPhoneNumber() {
        return this.workPhoneNumberInput.getText().toString();
    }

    @Override
    public String getEmail() {
        return this.emailInput.getText().toString();
    }

    @Override
    public String getChildSocialSecurityNumber() {
        return this.childSocialSecurityNumber.getText().toString();
    }

    @Override
    public String getRole() {
        return this.roleSpinner.getSelectedItem().toString();
    }

    @Override
    public void onDestroy() {
        this.presenter.onDestroy();
        super.onDestroy();
    }
}
