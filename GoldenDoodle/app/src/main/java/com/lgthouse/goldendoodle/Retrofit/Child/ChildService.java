package com.lgthouse.goldendoodle.Retrofit.Child;

import com.lgthouse.goldendoodle.POJO.Child;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Nesho on 2017-05-05.
 */

public interface ChildService {
    @GET("children")
    Call<List<Child>> getChildren();

    @GET("children/{socialSecurityNumber}")
    Call<Child> getChild(@Path("socialSecurityNumber") String socialSecurityNumber);

    @POST("children/{sectionName}")
    Call<Child> postChild(@Path("sectionName") String sectionName, @Body Child child);

    @POST("children/{socialSecurityNumber}/delete")
    Call<Child> deleteChild(@Path("socialSecurityNumber") String socialSecurityNumber, @Body Child child);

    @POST("children/update")
    Call<Child> updateChild(@Body Child child);
}
