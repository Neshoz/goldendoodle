package com.lgthouse.goldendoodle;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.widget.Button;
import android.widget.EditText;

import com.lgthouse.goldendoodle.Panel.PanelActivity;

public class LoginActivity extends Activity implements LoginMvp.View {

    private EditText usernameInput;
    private EditText passwordInput;
    private Button loginButton;
    private ProgressDialog progressDialog;
    private LoginMvp.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        this.presenter = new LoginPresenter(this);

        this.usernameInput = (EditText)findViewById(R.id.login_username_input);
        this.passwordInput = (EditText)findViewById(R.id.login_password_input);
        this.loginButton = (Button)findViewById(R.id.login_submit);

        this.loginButton.setOnClickListener(view -> presenter.validateCredentials());
    }

    @Override
    public void showProgress() {
        this.progressDialog = ProgressDialog.show(this, "", getString(R.string.authenticating_progress_text), false);
    }

    @Override
    public void hideProgress() {
        this.progressDialog.dismiss();
    }

    @Override
    public void showConnectionFailed() {
        Snackbar snackbar = Snackbar.make(this.findViewById(R.id.activity_login), getString(R.string.connectionFailed), Snackbar.LENGTH_LONG)
                .setAction(getString(R.string.retryConnection), view -> this.presenter.validateCredentials());
        snackbar.show();
    }

    @Override
    public void navigateToPanel() {
        this.startActivity(new Intent(this, PanelActivity.class));
    }

    @Override
    public void showUsernameError() {
        this.usernameInput.setError(getString(R.string.usernameError));
    }

    @Override
    public void showPasswordError() {
        this.passwordInput.setError(getString(R.string.passwordError));
    }

    @Override
    public String getUserName() {
        if(!this.usernameInput.getText().toString().equals("")) {
            return this.usernameInput.getText().toString();
        }
        return null;
    }

    @Override
    public String getPassword() {
        if(!this.passwordInput.getText().toString().equals("")) {
            return this.passwordInput.getText().toString();
        }
        return null;
    }

    @Override
    public void onDestroy() {
        this.presenter.onDestroy();
        super.onDestroy();
    }
}
