package com.lgthouse.goldendoodle.Panel.Admin.add;

/**
 * Created by Nesho on 2017-05-09.
 */

public interface AddAdminMvp {

    interface View {
        void showPostProgress();
        void hideProgress();
        void showConnectionFailed();
        void navigateToPanel();
        String getFirstName();
        String getLastName();
        String getUsername();
        String getPassword();
        String getEmail();
    }

    interface Presenter {
        void postAdmin();
        void onDestroy();
    }
}
