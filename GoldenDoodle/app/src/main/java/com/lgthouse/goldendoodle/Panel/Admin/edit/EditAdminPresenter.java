package com.lgthouse.goldendoodle.Panel.Admin.edit;

import com.lgthouse.goldendoodle.POJO.Admin;
import com.lgthouse.goldendoodle.Retrofit.Admin.AdminController;

/**
 * Created by Nesho on 2017-05-09.
 */

public class EditAdminPresenter implements EditAdminMvp.Presenter {

    private EditAdminMvp.View view;
    private AdminController adminController;

    public EditAdminPresenter(EditAdminMvp.View view) {
        this.view = view;
        this.adminController = new AdminController();
    }

    @Override
    public void updateAdmin() {
        Admin admin = this.view.getAdmin();
        admin.setFirstName(this.view.getFirstName());
        admin.setLastName(this.view.getLastName());
        admin.setUserName(this.view.getUsername());
        admin.setPassword(this.view.getPassword());
        admin.setEmail(this.view.getEmail());

        this.view.showPostProgress();
        this.adminController.updateAdmin(admin, new AdminController.OnAdminUpdatedCallback() {
            @Override
            public void onUpdateSuccess() {
                view.hideProgress();
                view.navigateToPanel();
            }

            @Override
            public void onUpdateFailure() {
                view.hideProgress();
                view.showPostConnectionFailed();
            }
        });
    }

    @Override
    public void loadAdmin() {
        this.view.showDownloadProgress();
        this.adminController.getAdmin(this.view.getIntentUsername(), new AdminController.OnAdminLoadedCallback() {
            @Override
            public void onAdminLoaded(Admin admin) {
                view.hideProgress();
                view.setAdmin(admin);
            }

            @Override
            public void onAdminLoadedFailure() {
                view.hideProgress();
                view.showDownloadConnectionFailed();
            }
        });
    }

    @Override
    public void onDestroy() {
        this.view = null;
    }
}
