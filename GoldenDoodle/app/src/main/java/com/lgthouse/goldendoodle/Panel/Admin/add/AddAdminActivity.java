package com.lgthouse.goldendoodle.Panel.Admin.add;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.widget.Button;
import android.widget.EditText;

import com.lgthouse.goldendoodle.Panel.PanelActivity;
import com.lgthouse.goldendoodle.R;

public class AddAdminActivity extends Activity implements AddAdminMvp.View {

    private EditText firstNameInput, lastNameInput, usernameInput, passwordInput, emailInput;
    private Button confirmButton;
    private AddAdminMvp.Presenter presenter;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_admin);
        this.presenter = new AddAdminPresenter(this);

        this.firstNameInput = (EditText)findViewById(R.id.add_admin_first_name_editText);
        this.lastNameInput = (EditText)findViewById(R.id.add_admin_last_name_editText);
        this.usernameInput = (EditText)findViewById(R.id.add_admin_username_editText);
        this.passwordInput = (EditText)findViewById(R.id.add_admin_password_editText);
        this.emailInput = (EditText)findViewById(R.id.add_admin_email_editText);
        this.confirmButton = (Button)findViewById(R.id.add_admin_confirm_button);

        this.confirmButton.setOnClickListener(view -> this.presenter.postAdmin());
    }

    @Override
    public void showPostProgress() {
        this.progressDialog = ProgressDialog.show(this, "", getString(R.string.postPDText), false);
    }

    @Override
    public void hideProgress() {
        this.progressDialog.dismiss();
    }

    @Override
    public void showConnectionFailed() {
        Snackbar snackbar = Snackbar.make(this.findViewById(R.id.activity_add_admin), getString(R.string.connectionFailed), Snackbar.LENGTH_LONG)
                .setAction(getString(R.string.retryConnection), view -> this.presenter.postAdmin());
        snackbar.show();
    }

    @Override
    public void navigateToPanel() {
        this.startActivity(new Intent(this, PanelActivity.class));
    }

    @Override
    public String getFirstName() {
        return this.firstNameInput.getText().toString();
    }

    @Override
    public String getLastName() {
        return this.lastNameInput.getText().toString();
    }

    @Override
    public String getUsername() {
        return this.usernameInput.getText().toString();
    }

    @Override
    public String getPassword() {
        return this.passwordInput.getText().toString();
    }

    @Override
    public String getEmail() {
        return this.emailInput.getText().toString();
    }

    @Override
    public void onDestroy() {
        this.presenter.onDestroy();
        super.onDestroy();
    }
}
