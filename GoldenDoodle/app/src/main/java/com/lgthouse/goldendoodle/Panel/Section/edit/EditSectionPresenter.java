package com.lgthouse.goldendoodle.Panel.Section.edit;

import com.lgthouse.goldendoodle.POJO.Section;
import com.lgthouse.goldendoodle.Retrofit.Section.SectionController;

/**
 * Created by Nesho on 2017-05-09.
 */

public class EditSectionPresenter implements EditSectionMvp.Presenter {

    private EditSectionMvp.View view;
    private SectionController sectionController;

    public EditSectionPresenter(EditSectionMvp.View view) {
        this.view = view;
        this.sectionController = new SectionController();
    }
    @Override
    public void loadSection() {
        this.view.showDownloadProgress();
        this.sectionController.getSection(this.view.getIntentSectionName(), new SectionController.OnSectionLoadedCallback() {
            @Override
            public void onSectionLoaded(Section section) {
                view.hideProgress();
                view.setSection(section);
            }

            @Override
            public void onSectionLoadedFailure() {
                view.hideProgress();
                view.showDownloadConnectionFailed();
            }
        });
    }

    @Override
    public void updateSection() {
        Section section = this.view.getSection();
        section.setSectionName(this.view.getSectionName());
        section.setPassword(this.view.getPassword());

        this.view.showPostProgress();
        this.sectionController.updateSection(section, new SectionController.OnSectionUpdatedCallback() {
            @Override
            public void onUpdateSuccess() {
                view.hideProgress();
                view.navigateToPanel();
            }

            @Override
            public void onUpdateFailure() {
                view.hideProgress();
                view.showPostConnectionFailed();
            }
        });
    }

    @Override
    public void onDestroy() {
        this.view = null;
    }
}
