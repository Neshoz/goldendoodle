package com.lgthouse.goldendoodle.Panel.Admin.edit;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.widget.Button;
import android.widget.EditText;

import com.lgthouse.goldendoodle.POJO.Admin;
import com.lgthouse.goldendoodle.Panel.PanelActivity;
import com.lgthouse.goldendoodle.R;

public class EditAdminActivity extends Activity implements EditAdminMvp.View {

    private static final String INTENT_EXTRA_USERNAME = "USERNAME";

    public static final Intent newIntent(Context caller, String username) {
        Intent intent = new Intent(caller, EditAdminActivity.class);
        intent.putExtra(INTENT_EXTRA_USERNAME, username);
        return intent;
    }

    private EditText firstNameInput, lastNameInput, usernameInput, passwordInput, emailInput;
    private ProgressDialog progressDialog;
    private Button confirmButton;
    private EditAdminMvp.Presenter presenter;
    private Admin downloadedAdmin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_admin);
        this.presenter = new EditAdminPresenter(this);

        this.firstNameInput = (EditText)findViewById(R.id.edit_admin_first_name_editText);
        this.lastNameInput = (EditText)findViewById(R.id.edit_admin_last_name_editText);
        this.usernameInput = (EditText)findViewById(R.id.edit_admin_username_editText);
        this.passwordInput = (EditText)findViewById(R.id.edit_admin_password_editText);
        this.emailInput = (EditText)findViewById(R.id.edit_admin_email_editText);
        this.confirmButton = (Button)findViewById(R.id.edit_admin_confirm_button);

        this.confirmButton.setOnClickListener(view -> this.presenter.updateAdmin());

        this.presenter.loadAdmin();
    }

    @Override
    public void setAdmin(Admin admin) {
        this.downloadedAdmin = admin;
        this.firstNameInput.setText(admin.getFirstName());
        this.lastNameInput.setText(admin.getLastName());
        this.usernameInput.setText(admin.getUserName());
        this.passwordInput.setText(admin.getPassword());
        this.emailInput.setText(admin.getEmail());
    }

    @Override
    public void showDownloadProgress() {
        this.progressDialog = ProgressDialog.show(this, "", getString(R.string.downloadPDText), false);
    }

    @Override
    public void showPostProgress() {
        this.progressDialog = ProgressDialog.show(this, "", getString(R.string.postPDText), false);
    }

    @Override
    public void hideProgress() {
        this.progressDialog.dismiss();
    }

    @Override
    public void showPostConnectionFailed() {
        Snackbar snackbar = Snackbar.make(this.findViewById(R.id.activity_edit_admin), getString(R.string.connectionFailed), Snackbar.LENGTH_LONG)
                .setAction(getString(R.string.retryConnection), view -> this.presenter.updateAdmin());
        snackbar.show();
    }

    @Override
    public void showDownloadConnectionFailed() {
        Snackbar snackbar = Snackbar.make(this.findViewById(R.id.activity_edit_admin), getString(R.string.connectionFailed), Snackbar.LENGTH_LONG)
                .setAction(getString(R.string.retryConnection), view -> this.presenter.loadAdmin());
        snackbar.show();
    }

    @Override
    public void navigateToPanel() {
        this.startActivity(new Intent(this, PanelActivity.class));
    }

    @Override
    public String getFirstName() {
        return this.firstNameInput.getText().toString();
    }

    @Override
    public String getLastName() {
        return this.lastNameInput.getText().toString();
    }

    @Override
    public String getUsername() {
        return this.usernameInput.getText().toString();
    }

    @Override
    public String getPassword() {
        return this.passwordInput.getText().toString();
    }

    @Override
    public String getEmail() {
        return this.emailInput.getText().toString();
    }

    @Override
    public String getIntentUsername() {
        return this.getIntent().getStringExtra(INTENT_EXTRA_USERNAME);
    }

    @Override
    public Admin getAdmin() {
        return this.downloadedAdmin;
    }

    @Override
    public void onDestroy() {
        this.presenter.onDestroy();
        super.onDestroy();
    }
}
