package com.lgthouse.goldendoodle.Panel;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.lgthouse.goldendoodle.Panel.Admin.AdminFragment;
import com.lgthouse.goldendoodle.Panel.Adult.AdultFragment;
import com.lgthouse.goldendoodle.Panel.Child.ChildFragment;
import com.lgthouse.goldendoodle.Panel.Section.SectionFragment;
import com.lgthouse.goldendoodle.Panel.Teacher.TeacherFragment;
import com.lgthouse.goldendoodle.R;

/**
 * Created by Nesho on 2017-05-08.
 */

public class PanelPagerAdapter extends FragmentPagerAdapter {

    private static final int NUMBER_OF_FRAGMENTS = 5;
    private Context context;

    public PanelPagerAdapter(FragmentManager fragmentManager, Context context) {
        super(fragmentManager);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new AdminFragment();
            case 1:
                return new AdultFragment();
            case 2:
                return new ChildFragment();
            case 3:
                return new SectionFragment();
            case 4:
                return new TeacherFragment();
            default:
                return null;
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return this.context.getString(R.string.adminFragmentTabTitle);
            case 1:
                return this.context.getString(R.string.adultFragmentTabTitle);
            case 2:
                return this.context.getString(R.string.childFragmentTabTitle);
            case 3:
                return this.context.getString(R.string.sectionFragmentTabTitle);
            case 4:
                return this.context.getString(R.string.teacherFragmentTabTitle);
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return NUMBER_OF_FRAGMENTS;
    }
}
