package com.lgthouse.goldendoodle.Panel.Admin.edit;

import com.lgthouse.goldendoodle.POJO.Admin;

/**
 * Created by Nesho on 2017-05-09.
 */

public interface EditAdminMvp {

    interface View {
        void setAdmin(Admin admin);
        void showDownloadProgress();
        void showPostProgress();
        void hideProgress();
        void showPostConnectionFailed();
        void showDownloadConnectionFailed();
        void navigateToPanel();
        String getFirstName();
        String getLastName();
        String getUsername();
        String getPassword();
        String getEmail();
        String getIntentUsername();
        Admin getAdmin();
    }

    interface Presenter {
        void updateAdmin();
        void loadAdmin();
        void onDestroy();
    }
}
