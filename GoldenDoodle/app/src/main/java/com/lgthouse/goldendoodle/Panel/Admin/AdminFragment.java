package com.lgthouse.goldendoodle.Panel.Admin;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.lgthouse.goldendoodle.POJO.Admin;
import com.lgthouse.goldendoodle.Panel.Admin.add.AddAdminActivity;
import com.lgthouse.goldendoodle.Panel.Admin.edit.EditAdminActivity;
import com.lgthouse.goldendoodle.R;

import java.util.List;

/**
 * Created by Nesho on 2017-05-08.
 */

public class AdminFragment extends Fragment implements AdminMvp.View, AdminAdapter.OnDeleteClickedListener, AdminAdapter.OnEditClickedListener {

    private AdminAdapter adminAdapter;
    private AdminMvp.Presenter presenter;
    private ProgressBar progressBar;
    private RecyclerView recyclerView;
    private FloatingActionButton addButton;
    private Admin clickedAdmin;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_admin, container, false);

        this.presenter = new AdminPresenter(this);
        this.adminAdapter = new AdminAdapter(this.getContext(), this, this);

        this.progressBar = (ProgressBar)view.findViewById(R.id.admin_fragment_loading_spinner);
        this.recyclerView = (RecyclerView)view.findViewById(R.id.admin_list);
        this.addButton = (FloatingActionButton)view.findViewById(R.id.admin_add);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this.getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adminAdapter);

        this.addButton.setOnClickListener(btn -> this.presenter.navigateToAdd());

        this.presenter.loadAdmins();

        return view;
    }

    @Override
    public void setAdmins(List<Admin> admins) {
        this.adminAdapter.setItems(admins);
    }

    @Override
    public void removeAdmin(Admin admin) {
        this.adminAdapter.removeItem(admin);
    }

    @Override
    public void showProgress() {
        this.recyclerView.setVisibility(View.GONE);
        this.progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        this.progressBar.setVisibility(View.GONE);
        this.recyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showConnectionFailed() {
        Snackbar snackbar = Snackbar.make(this.getActivity().findViewById(R.id.activity_panel), getString(R.string.connectionFailed), Snackbar.LENGTH_LONG)
                .setAction(getString(R.string.retryConnection), view -> this.presenter.loadAdmins());
        snackbar.show();
    }

    @Override
    public void showDeleteConnectionFailed() {
        Snackbar snackbar = Snackbar.make(this.getActivity().findViewById(R.id.activity_panel), getString(R.string.connectionFailed), Snackbar.LENGTH_LONG)
                .setAction(getString(R.string.retryConnection), view -> this.presenter.removeAdmin(this.clickedAdmin));
        snackbar.show();
    }

    @Override
    public void navigateToEdit(Admin admin) {
        this.startActivity(EditAdminActivity.newIntent(this.getContext(), admin.getUserName()));
    }

    @Override
    public void navigateToAdd() {
        this.startActivity(new Intent(this.getContext(), AddAdminActivity.class));
    }

    @Override
    public void onDeleteClicked(Admin admin) {
        this.clickedAdmin = admin;
        this.presenter.removeAdmin(admin);
    }

    @Override
    public void onEditClicked(Admin admin) {
        this.presenter.navigateToEdit(admin);
    }

    @Override
    public void onDestroy() {
        this.presenter.onDestroy();
        super.onDestroy();
    }
}
