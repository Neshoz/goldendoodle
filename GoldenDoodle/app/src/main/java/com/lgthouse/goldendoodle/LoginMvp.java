package com.lgthouse.goldendoodle;

/**
 * Created by Nesho on 2017-05-05.
 */

public interface LoginMvp {

    interface View {
        void showProgress();
        void hideProgress();
        void showConnectionFailed();
        void navigateToPanel();
        void showUsernameError();
        void showPasswordError();
        String getUserName();
        String getPassword();
    }

    interface Presenter {
        void validateCredentials();
        void onDestroy();
    }
}
