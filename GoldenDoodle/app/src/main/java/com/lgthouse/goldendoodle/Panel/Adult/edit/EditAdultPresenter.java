package com.lgthouse.goldendoodle.Panel.Adult.edit;

import com.lgthouse.goldendoodle.POJO.Adult;
import com.lgthouse.goldendoodle.Retrofit.Adult.AdultController;

/**
 * Created by Nesho on 2017-05-08.
 */

public class EditAdultPresenter implements EditAdultMvp.Presenter {

    private EditAdultMvp.View view;
    private AdultController adultController;

    public EditAdultPresenter(EditAdultMvp.View view) {
        this.view = view;
        this.adultController = new AdultController();
    }

    @Override
    public void loadAdult() {
        this.view.showLoadProgress();
        this.adultController.loadAdult(this.view.getAdultSSN(), new AdultController.OnAdultLoadedCallback() {
            @Override
            public void onAdultLoaded(Adult adult) {
                view.hideProgress();
                view.setAdult(adult);
            }

            @Override
            public void onAdultLoadedFailure() {
                view.hideProgress();
                view.showDownloadConnectionFailed();
            }
        });
    }

    @Override
    public void postChanges() {
        Adult adult = this.view.getAdult();
        adult.setFirstName(this.view.getFirstName());
        adult.setLastName(this.view.getLastName());
        adult.setPhoneNumber(this.view.getPhoneNumber());
        adult.setWorkPhoneNumber(this.view.getWorkPhoneNumber());
        adult.setEmail(this.view.getEmail());

        this.view.showPostProgress();
        this.adultController.updateAdult(this.view.getAdultSSN(), adult, new AdultController.OnAdultUpdateCallback() {
            @Override
            public void onUpdateSuccess() {
                view.hideProgress();
                view.navigateToPanel();
            }

            @Override
            public void onUpdateFailure() {
                view.hideProgress();
                view.showPostConnectionFailed();
            }
        });
    }

    @Override
    public void onDestroy() {
        this.view = null;
    }
}
