package com.lgthouse.goldendoodle.Panel.Adult;

import com.lgthouse.goldendoodle.POJO.Adult;
import com.lgthouse.goldendoodle.Retrofit.Adult.AdultController;

import java.util.List;

/**
 * Created by Nesho on 2017-05-08.
 */

public class AdultPresenter implements AdultMvp.Presenter {

    private AdultMvp.View view;
    private AdultController adultController;

    public AdultPresenter(AdultMvp.View view) {
        this.view = view;
        this.adultController = new AdultController();
    }

    @Override
    public void loadAdults() {
        this.view.showProgress();
        this.adultController.getAllAdults(new AdultController.OnAdultsLoadedCallback() {
            @Override
            public void onAdultsLoaded(List<Adult> adults) {
                view.hideProgress();
                view.setAdults(adults);
            }

            @Override
            public void onAdultsLoadedFailure() {
                view.hideProgress();
                view.showConnectionFailed();
            }
        });
    }

    @Override
    public void removeAdult(Adult adult) {
        this.view.showProgress();
        this.adultController.deleteAdult(adult.getSocialSecurityNumber(), adult, new AdultController.OnDeleteAdultCallback() {
            @Override
            public void onDeleteSuccess() {
                view.hideProgress();
                view.removeAdult(adult);
            }

            @Override
            public void onDeleteFailure() {
                view.hideProgress();
                view.showDeleteConnectionFailed();
            }
        });
    }

    @Override
    public void navigateToEdit(Adult adult) {
        this.view.navigateToEdit(adult);
    }

    @Override
    public void navigateToAdd() {
        this.view.navigateToAdd();
    }

    @Override
    public void onDestroy() {
        this.view = null;
    }
}
