package com.lgthouse.goldendoodle.Panel.Child.add;

import com.lgthouse.goldendoodle.POJO.Section;

import java.util.List;

/**
 * Created by Nesho on 2017-05-09.
 */

public interface AddChildMvp {

    interface View {
        void setSections(List<Section> sections);
        void showPostProgress();
        void hideProgress();
        void showPostConnectionFailed();
        void navigateToPanel();
        String getFirstName();
        String getLastName();
        String getSocialSecurityNumber();
        Section getSection();
    }

    interface Presenter {
        void postChild();
        void loadSections();
        void onDestroy();
    }
}
