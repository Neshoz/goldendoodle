package com.lgthouse.goldendoodle.Panel.Admin;

import com.lgthouse.goldendoodle.POJO.Admin;

import java.util.List;

/**
 * Created by Nesho on 2017-05-08.
 */

public interface AdminMvp {

    interface View {
        void setAdmins(List<Admin> admins);
        void removeAdmin(Admin admin);
        void showProgress();
        void hideProgress();
        void showConnectionFailed();
        void showDeleteConnectionFailed();
        void navigateToEdit(Admin admin);
        void navigateToAdd();
    }

    interface Presenter {
        void loadAdmins();
        void removeAdmin(Admin admin);
        void navigateToEdit(Admin admin);
        void navigateToAdd();
        void onDestroy();
    }
}
