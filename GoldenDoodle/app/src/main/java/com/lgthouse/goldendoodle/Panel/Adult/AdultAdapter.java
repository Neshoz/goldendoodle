package com.lgthouse.goldendoodle.Panel.Adult;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.lgthouse.goldendoodle.POJO.Adult;
import com.lgthouse.goldendoodle.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nesho on 2017-05-08.
 */

public class AdultAdapter extends RecyclerView.Adapter<AdultAdapter.ViewHolder> {

    private Context context;
    private List<Adult> adults;
    private OnDeleteClickListener deleteClickListener;
    private OnEditClickedListener editClickedListener;

    public AdultAdapter(Context context, OnDeleteClickListener deleteClickListener, OnEditClickedListener editClickedListener) {
        this.context = context;
        this.adults = new ArrayList<>();
        this.deleteClickListener = deleteClickListener;
        this.editClickedListener = editClickedListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Adult adult = this.adults.get(position);

        holder.itemView.setTag(adult);
        holder.editImageView.setTag(adult);
        holder.deleteImageView.setTag(adult);

        holder.firstNameTextView.setText(adult.getFirstName());
        holder.lastNameTextView.setText(adult.getLastName());
        holder.ssnTextView.setText(adult.getSocialSecurityNumber());

        holder.editImageView.setOnClickListener(view -> {
            Adult clickedAdult = (Adult)view.getTag();
            editClickedListener.onEditClicked(clickedAdult);
        });

        holder.deleteImageView.setOnClickListener(view -> {
            Adult clickedAdult = (Adult)view.getTag();
            deleteClickListener.onDeleteClicked(clickedAdult);
        });
    }

    @Override
    public int getItemCount() {
        return this.adults.size();
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView firstNameTextView;
        private TextView lastNameTextView;
        private TextView ssnTextView;
        private ImageView editImageView;
        private ImageView deleteImageView;

        public ViewHolder(View itemView) {
            super(itemView);
            this.firstNameTextView = (TextView)itemView.findViewById(R.id.item_first_name_textView);
            this.lastNameTextView = (TextView)itemView.findViewById(R.id.item_last_name_textView);
            this.ssnTextView = (TextView)itemView.findViewById(R.id.item_ssn_textView);
            this.editImageView = (ImageView)itemView.findViewById(R.id.item_edit_icon);
            this.deleteImageView = (ImageView)itemView.findViewById(R.id.item_delete_icon);
        }
    }

    public interface OnDeleteClickListener {
        void onDeleteClicked(Adult adult);
    }

    public interface OnEditClickedListener {
        void onEditClicked(Adult adult);
    }

    public void setItems(List<Adult> items) {
        this.adults.clear();
        this.adults.addAll(items);
        this.notifyDataSetChanged();
    }

    public void removeItem(Adult adult) {
        this.adults.remove(adult);
        this.notifyDataSetChanged();
    }
}
