package com.lgthouse.goldendoodle.Panel.Admin.add;

import com.lgthouse.goldendoodle.POJO.Admin;
import com.lgthouse.goldendoodle.Retrofit.Admin.AdminController;

/**
 * Created by Nesho on 2017-05-09.
 */

public class AddAdminPresenter implements AddAdminMvp.Presenter {

    private AddAdminMvp.View view;
    private AdminController adminController;

    public AddAdminPresenter(AddAdminMvp.View view) {
        this.view = view;
        this.adminController = new AdminController();
    }

    @Override
    public void postAdmin() {
        Admin admin = new Admin();
        admin.setFirstName(this.view.getFirstName());
        admin.setLastName(this.view.getLastName());
        admin.setUserName(this.view.getUsername());
        admin.setPassword(this.view.getPassword());
        admin.setEmail(this.view.getEmail());

        this.view.showPostProgress();
        this.adminController.postAdmin(admin, new AdminController.OnAdminPostedCallback() {
            @Override
            public void onPostSuccess() {
                view.hideProgress();
                view.navigateToPanel();
            }

            @Override
            public void onPostFailure() {
                view.hideProgress();
                view.showConnectionFailed();
            }
        });
    }

    @Override
    public void onDestroy() {
        this.view = null;
    }
}
