package com.lgthouse.goldendoodle.Panel;

/**
 * Created by Nesho on 2017-05-08.
 */

public class PanelPresenter implements PanelMvp.Presenter {

    private PanelMvp.View view;

    public PanelPresenter(PanelMvp.View view) {
        this.view = view;
    }

    @Override
    public void onDestroy() {
        this.view = null;
    }
}
