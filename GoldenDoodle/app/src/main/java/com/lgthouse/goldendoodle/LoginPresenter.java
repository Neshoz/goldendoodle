package com.lgthouse.goldendoodle;

import com.lgthouse.goldendoodle.POJO.Admin;
import com.lgthouse.goldendoodle.Retrofit.Admin.AdminController;

/**
 * Created by Nesho on 2017-05-05.
 */

public class LoginPresenter implements LoginMvp.Presenter {

    private LoginMvp.View view;
    private AdminController adminController;

    public LoginPresenter(LoginMvp.View view) {
        this.view = view;
        this.adminController = new AdminController();
    }

    @Override
    public void validateCredentials() {
        this.view.showProgress();
        this.adminController.getAdmin(this.view.getUserName(), new AdminController.OnAdminLoadedCallback() {
            @Override
            public void onAdminLoaded(Admin admin) {
                if(admin != null && !admin.getPassword().equals(view.getPassword())) {
                    view.hideProgress();
                    view.showPasswordError();
                }
                else if(admin == null) {
                    view.hideProgress();
                    view.showUsernameError();
                }
                else {
                    view.hideProgress();
                    view.navigateToPanel();
                }
            }

            @Override
            public void onAdminLoadedFailure() {
                view.hideProgress();
                view.showConnectionFailed();
            }
        });
    }

    @Override
    public void onDestroy() {
        this.view = null;
    }
}
