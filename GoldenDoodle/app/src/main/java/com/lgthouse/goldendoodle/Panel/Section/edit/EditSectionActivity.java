package com.lgthouse.goldendoodle.Panel.Section.edit;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.widget.Button;
import android.widget.EditText;

import com.lgthouse.goldendoodle.POJO.Section;
import com.lgthouse.goldendoodle.Panel.PanelActivity;
import com.lgthouse.goldendoodle.R;

public class EditSectionActivity extends Activity implements EditSectionMvp.View {

    private static final String INTENT_EXTRA_STRING_SECTION_NAME = "SECTION_NAME";

    public static final Intent newIntent(Context caller, String sectionName) {
        Intent intent = new Intent(caller, EditSectionActivity.class);
        intent.putExtra(INTENT_EXTRA_STRING_SECTION_NAME, sectionName);
        return intent;
    }

    private EditText sectionNameInput, passwordInput;
    private Button confirmButton;
    private EditSectionMvp.Presenter presenter;
    private ProgressDialog progressDialog;
    private Section downloadedSection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_section);

        this.presenter = new EditSectionPresenter(this);

        this.sectionNameInput = (EditText)findViewById(R.id.edit_section_name_editText);
        this.passwordInput = (EditText)findViewById(R.id.edit_section_password_editText);
        this.confirmButton = (Button)findViewById(R.id.edit_section_confirm_button);

        this.confirmButton.setOnClickListener(view -> this.presenter.updateSection());

        this.presenter.loadSection();
    }

    @Override
    public void setSection(Section section) {
        this.downloadedSection = section;
        this.sectionNameInput.setText(section.getSectionName());
        this.passwordInput.setText(section.getPassword());
    }

    @Override
    public void showDownloadProgress() {
        this.progressDialog = ProgressDialog.show(this, "", getString(R.string.downloadPDText), false);
    }

    @Override
    public void showPostProgress() {
        this.progressDialog = ProgressDialog.show(this, "", getString(R.string.postPDText), false);
    }

    @Override
    public void hideProgress() {
        this.progressDialog.dismiss();
    }

    @Override
    public void showDownloadConnectionFailed() {
        Snackbar snackbar = Snackbar.make(this.findViewById(R.id.activity_edit_section), getString(R.string.connectionFailed), Snackbar.LENGTH_LONG)
                .setAction(getString(R.string.retryConnection), view -> this.presenter.loadSection());
        snackbar.show();
    }

    @Override
    public void showPostConnectionFailed() {
        Snackbar snackbar = Snackbar.make(this.findViewById(R.id.activity_edit_section), getString(R.string.connectionFailed), Snackbar.LENGTH_LONG)
                .setAction(getString(R.string.retryConnection), view -> this.presenter.updateSection());
        snackbar.show();
    }

    @Override
    public void navigateToPanel() {
        this.startActivity(new Intent(this, PanelActivity.class));
    }

    @Override
    public String getSectionName() {
        return this.sectionNameInput.getText().toString();
    }

    @Override
    public String getPassword() {
        return this.passwordInput.getText().toString();
    }

    @Override
    public Section getSection() {
        return this.downloadedSection;
    }

    @Override
    public String getIntentSectionName() {
        return this.getIntent().getStringExtra(INTENT_EXTRA_STRING_SECTION_NAME);
    }

    @Override
    public void onDestroy() {
        this.presenter.onDestroy();
        super.onDestroy();
    }
}
