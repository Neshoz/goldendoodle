package com.lgthouse.goldendoodle.Panel.Adult.edit;

import com.lgthouse.goldendoodle.POJO.Adult;

/**
 * Created by Nesho on 2017-05-08.
 */

public interface EditAdultMvp {

    interface View {
        void setAdult(Adult adult);
        void showLoadProgress();
        void showPostProgress();
        void hideProgress();
        void showDownloadConnectionFailed();
        void showPostConnectionFailed();
        void navigateToPanel();
        String getAdultSSN();
        String getFirstName();
        String getLastName();
        String getPhoneNumber();
        String getWorkPhoneNumber();
        String getEmail();
        Adult getAdult();
    }

    interface Presenter {
        void loadAdult();
        void postChanges();
        void onDestroy();
    }
}
