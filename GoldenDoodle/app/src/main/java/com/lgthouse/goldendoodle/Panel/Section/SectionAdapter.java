package com.lgthouse.goldendoodle.Panel.Section;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.lgthouse.goldendoodle.POJO.Section;
import com.lgthouse.goldendoodle.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nesho on 2017-05-08.
 */

public class SectionAdapter extends RecyclerView.Adapter<SectionAdapter.ViewHolder> {

    private Context context;
    private List<Section> sections;
    private OnEditClickListener editClickListener;

    public SectionAdapter(Context context, OnEditClickListener editClickListener) {
        this.context = context;
        this.sections = new ArrayList<>();
        this.editClickListener = editClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Section section = this.sections.get(position);

        holder.firstNameTextView.setText(section.getSectionName());
        holder.itemView.setTag(section);
        holder.editIcon.setTag(section);
        holder.deleteIcon.setTag(section);

        holder.editIcon.setOnClickListener(view -> {
            Section clickedSection = (Section)view.getTag();
            editClickListener.onEditClicked(clickedSection);
        });
    }

    @Override
    public int getItemCount() {
        return this.sections.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView firstNameTextView;
        private TextView minusTextView;
        private ImageView deleteIcon;
        private ImageView editIcon;

        public ViewHolder(View itemView) {
            super(itemView);
            this.firstNameTextView = (TextView)itemView.findViewById(R.id.item_first_name_textView);
            this.minusTextView = (TextView)itemView.findViewById(R.id.textView_minus);
            this.deleteIcon = (ImageView)itemView.findViewById(R.id.item_delete_icon);
            this.editIcon = (ImageView)itemView.findViewById(R.id.item_edit_icon);

            this.deleteIcon.setVisibility(View.GONE);
            this.minusTextView.setVisibility(View.GONE);
        }
    }

    public interface OnEditClickListener {
        void onEditClicked(Section section);
    }

    public void setItems(List<Section> sections) {
        this.sections.clear();
        this.sections.addAll(sections);
        this.notifyDataSetChanged();
    }
}
