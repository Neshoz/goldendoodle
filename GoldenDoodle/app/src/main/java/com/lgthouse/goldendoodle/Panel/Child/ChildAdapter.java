package com.lgthouse.goldendoodle.Panel.Child;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.lgthouse.goldendoodle.POJO.Child;
import com.lgthouse.goldendoodle.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nesho on 2017-05-08.
 */

public class ChildAdapter extends RecyclerView.Adapter<ChildAdapter.ViewHolder> {

    private Context context;
    private List<Child> children;
    private OnDeleteClickListener deleteClickListener;
    private OnEditClickListener editClickListener;

    public ChildAdapter(Context context, OnDeleteClickListener deleteClickListener, OnEditClickListener editClickListener) {
        this.context = context;
        this.children = new ArrayList<>();
        this.deleteClickListener = deleteClickListener;
        this.editClickListener = editClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Child child = this.children.get(position);

        holder.itemView.setTag(child);
        holder.editImageView.setTag(child);
        holder.deleteImageView.setTag(child);

        holder.firstNameTextView.setText(child.getFirstName());
        holder.lastNameTextView.setText(child.getLastName());
        holder.ssnTextView.setText(child.getSocialSecurityNumber());

        holder.editImageView.setOnClickListener(view -> {
            Child clickedChild = (Child)view.getTag();
            editClickListener.onEditClicked(clickedChild);
        });

        holder.deleteImageView.setOnClickListener(view -> {
            Child clickedChild = (Child)view.getTag();
            deleteClickListener.onDeleteClicked(clickedChild);
        });
    }

    @Override
    public int getItemCount() {
        return this.children.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView firstNameTextView;
        private TextView lastNameTextView;
        private TextView ssnTextView;
        private ImageView deleteImageView;
        private ImageView editImageView;

        public ViewHolder(View itemView) {
            super(itemView);
            this.firstNameTextView = (TextView)itemView.findViewById(R.id.item_first_name_textView);
            this.lastNameTextView = (TextView)itemView.findViewById(R.id.item_last_name_textView);
            this.ssnTextView = (TextView)itemView.findViewById(R.id.item_ssn_textView);
            this.deleteImageView = (ImageView)itemView.findViewById(R.id.item_delete_icon);
            this.editImageView = (ImageView)itemView.findViewById(R.id.item_edit_icon);
        }
    }

    public interface OnDeleteClickListener {
        void onDeleteClicked(Child child);
    }

    public interface OnEditClickListener {
        void onEditClicked(Child child);
    }

    public void setItems(List<Child> items) {
        this.children.clear();
        this.children.addAll(items);
        this.notifyDataSetChanged();
    }

    public void removeItem(Child child) {
        this.children.remove(child);
        this.notifyDataSetChanged();
    }
}
