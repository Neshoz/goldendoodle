package com.lgthouse.goldendoodle.Panel.Teacher;

import com.lgthouse.goldendoodle.POJO.Teacher;
import com.lgthouse.goldendoodle.Retrofit.Teacher.TeacherController;

import java.util.List;

/**
 * Created by Nesho on 2017-05-08.
 */

public class TeacherPresenter implements TeacherMvp.Presenter {

    private TeacherMvp.View view;
    private TeacherController teacherController;

    public TeacherPresenter(TeacherMvp.View view) {
        this.view = view;
        this.teacherController = new TeacherController();
    }


    @Override
    public void loadTeachers() {
        this.view.showProgress();
        this.teacherController.getAllTeachers(new TeacherController.OnTeachersLoadedCallback() {
            @Override
            public void onTeachersLoaded(List<Teacher> teachers) {
                view.hideProgress();
                view.setTeachers(teachers);
            }

            @Override
            public void onTeachersLoadedFailure() {
                view.hideProgress();
                view.showConnectionFailed();
            }
        });
    }

    @Override
    public void deleteTeacher(Teacher teacher) {
        this.view.showProgress();
        this.teacherController.deleteTeacher(teacher.getSocialSecurityNumber(), teacher, new TeacherController.OnTeacherDeletedCallback() {
            @Override
            public void onDeleteSuccess() {
                view.hideProgress();
                view.deleteTeacher(teacher);
            }

            @Override
            public void onDeleteFailure() {
                view.hideProgress();
                view.showDeleteConnectionFailed();
            }
        });
    }

    @Override
    public void navigateToAdd() {
        this.view.navigateToAdd();
    }

    @Override
    public void navigateToEdit(Teacher teacher) {
        this.view.navigateToEdit(teacher);
    }

    @Override
    public void onDestroy() {
        this.view = null;
    }
}
