package com.lgthouse.goldendoodle.Panel;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;

import com.lgthouse.goldendoodle.R;

public class PanelActivity extends AppCompatActivity implements PanelMvp.View {

    private PanelMvp.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_panel);

        PanelPagerAdapter panelPagerAdapter = new PanelPagerAdapter(this.getSupportFragmentManager(), this);
        ViewPager viewPager = (ViewPager)findViewById(R.id.panel_viewpager);
        viewPager.setAdapter(panelPagerAdapter);

        TabLayout tabLayout = (TabLayout)findViewById(R.id.panel_tabs);
        tabLayout.setupWithViewPager(viewPager);

        this.presenter = new PanelPresenter(this);
    }

    @Override
    public void onBackPressed() {
        this.presenter.onDestroy();
        super.onBackPressed();
    }

    @Override
    public void onDestroy() {
        this.presenter.onDestroy();
        super.onDestroy();
    }
}
