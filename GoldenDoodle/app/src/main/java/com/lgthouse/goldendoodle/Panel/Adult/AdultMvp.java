package com.lgthouse.goldendoodle.Panel.Adult;

import com.lgthouse.goldendoodle.POJO.Adult;

import java.util.List;

/**
 * Created by Nesho on 2017-05-08.
 */

public interface AdultMvp {

    interface View {
        void setAdults(List<Adult> adults);
        void removeAdult(Adult adult);
        void showProgress();
        void hideProgress();
        void showConnectionFailed();
        void showDeleteConnectionFailed();
        void navigateToEdit(Adult adult);
        void navigateToAdd();
    }

    interface Presenter {
        void loadAdults();
        void removeAdult(Adult adult);
        void navigateToEdit(Adult adult);
        void navigateToAdd();
        void onDestroy();
    }
}
