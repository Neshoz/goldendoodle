package com.lgthouse.goldendoodle.Panel.Child;

import com.lgthouse.goldendoodle.POJO.Child;
import com.lgthouse.goldendoodle.Retrofit.Child.ChildController;

import java.util.List;

/**
 * Created by Nesho on 2017-05-08.
 */

public class ChildPresenter implements ChildMvp.Presenter {

    private ChildMvp.View view;
    private ChildController childController;

    public ChildPresenter(ChildMvp.View view) {
        this.view = view;
        this.childController = new ChildController();
    }

    @Override
    public void loadChildren() {
        this.view.showProgress();
        this.childController.getAllChildren(new ChildController.OnChildrenLoadedCallback() {
            @Override
            public void onChildrenLoaded(List<Child> children) {
                view.hideProgress();
                view.setChildren(children);
            }

            @Override
            public void onChildrenLoadedFailure() {
                view.hideProgress();
                view.showConnectionFailed();
            }
        });
    }

    @Override
    public void deleteChild(Child child) {
        this.view.showProgress();
        this.childController.deleteChild(child.getSocialSecurityNumber(), child, new ChildController.OnChildDeletedCallback() {
            @Override
            public void onDeleteSuccess() {
                view.hideProgress();
                view.removeChild(child);
            }

            @Override
            public void onDeleteFailure() {
                view.hideProgress();
                view.showDeleteConnectionFailed();
            }
        });
    }

    @Override
    public void navigateToAdd() {
        this.view.navigateToAdd();
    }

    @Override
    public void navigateToEdit(Child child) {
        this.view.navigateToEdit(child);
    }

    @Override
    public void onDestroy() {
        this.view = null;
    }
}
