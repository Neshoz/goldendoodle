package com.lgthouse.goldendoodle.Panel.Child.edit;

import com.lgthouse.goldendoodle.POJO.Child;
import com.lgthouse.goldendoodle.POJO.Section;
import com.lgthouse.goldendoodle.Retrofit.Child.ChildController;
import com.lgthouse.goldendoodle.Retrofit.Section.SectionController;

import java.util.List;

/**
 * Created by Nesho on 2017-05-09.
 */

public class EditChildPresenter implements EditChildMvp.Presenter {

    private EditChildMvp.View view;
    private ChildController childController;
    private SectionController sectionController;

    public EditChildPresenter(EditChildMvp.View view) {
        this.view = view;
        this.childController = new ChildController();
        this.sectionController = new SectionController();
    }

    @Override
    public void loadChild() {
        this.view.showDownloadProgress();
        this.childController.getChild(this.view.getSocialSecurityNumber(), new ChildController.OnChildLoadedCallback() {
            @Override
            public void onChildLoaded(Child child) {
                view.hideProgress();
                view.setChild(child);
            }

            @Override
            public void onChildLoadedFailure() {
                view.hideProgress();
                view.showDownloadConnectionFailed();
            }
        });
    }

    @Override
    public void loadSections() {
        this.sectionController.getAllSections(new SectionController.OnSectionsLoadedCallback() {
            @Override
            public void onSectionsLoaded(List<Section> sections) {
                view.setSections(sections);
            }

            @Override
            public void onSectionsLoadedFailure() {

            }
        });
    }

    @Override
    public void updateChild() {
        Child child = this.view.getChild();

        child.setFirstName(this.view.getFirstName());
        child.setLastName(this.view.getLastName());
        child.setSectionId(this.view.getSection().getSectionId());

        this.view.showPostProgress();
        this.childController.updateChild(child, new ChildController.OnChildUpdatedCallback() {
            @Override
            public void onUpdateSuccess() {
                view.hideProgress();
                view.navigateToPanel();
            }

            @Override
            public void onUpdateFailure() {
                view.hideProgress();
                view.showPostConnectionFailed();
            }
        });
    }

    @Override
    public void setSelectedSpinnerItem(List<Section> sections) {
        this.view.setSelectedSpinnerItem(sections);
    }

    @Override
    public void onDestroy() {
        this.view = null;
    }
}
