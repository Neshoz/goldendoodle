package com.lgthouse.goldendoodle.POJO;

/**
 * Created by Nesho on 2017-05-05.
 */

public class Adult {
    private long adultId;
    private String socialSecurityNumber;
    private String firstName;
    private String lastName;
    private String email;
    private String role;
    private String phoneNumber;
    private String workPhoneNumber;

    public Adult(String firstName, String lastName, String socialSecurityNumber, String email, String phoneNumber, String workPhoneNumber, String role){
        this.firstName = firstName;
        this.lastName = lastName;
        this.socialSecurityNumber = socialSecurityNumber;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.workPhoneNumber = workPhoneNumber;
        this.role = role;
    }
    public Adult(){}

    public long getAdultId() {
        return adultId;
    }
    public void setAdultId(long adultId) {
        this.adultId = adultId;
    }
    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }
    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getPhoneNumber() {
        return phoneNumber;
    }
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    public String getWorkPhoneNumber() {
        return workPhoneNumber;
    }
    public void setWorkPhoneNumber(String workPhoneNumber) {
        this.workPhoneNumber = workPhoneNumber;
    }
    public String getRole(){
        return role;
    }
    public void setRole(String role){
        this.role = role;
    }
}
