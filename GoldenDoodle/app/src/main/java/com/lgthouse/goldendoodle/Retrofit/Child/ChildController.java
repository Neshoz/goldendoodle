package com.lgthouse.goldendoodle.Retrofit.Child;

import com.lgthouse.goldendoodle.POJO.Child;
import com.lgthouse.goldendoodle.Retrofit.Api;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Nesho on 2017-05-05.
 */

public class ChildController {

    private static final int RESPONSE_OK = 200;
    private static final int RESPONSE_OK_NO_CONTENT = 204;

    private ChildService service;

    public ChildController() {
        this.service = Api.getInstance().getChildService();
    }

    public void getAllChildren(OnChildrenLoadedCallback callback) {
        Call<List<Child>> call = this.service.getChildren();
        call.enqueue(new Callback<List<Child>>() {
            @Override
            public void onResponse(Call<List<Child>> call, Response<List<Child>> response) {
                List<Child> children = response.body();
                callback.onChildrenLoaded(children);
            }

            @Override
            public void onFailure(Call<List<Child>> call, Throwable t) {
                t.printStackTrace();
                callback.onChildrenLoadedFailure();
            }
        });
    }

    public void getChild(String socialSecurityNumber, OnChildLoadedCallback callback) {
        Call<Child> call = this.service.getChild(socialSecurityNumber);
        call.enqueue(new Callback<Child>() {
            @Override
            public void onResponse(Call<Child> call, Response<Child> response) {
                Child child = response.body();
                callback.onChildLoaded(child);
            }

            @Override
            public void onFailure(Call<Child> call, Throwable t) {
                t.printStackTrace();
                callback.onChildLoadedFailure();
            }
        });
    }

    public void postChild(String sectionName, Child child, OnChildPostedCallback callback) {
        Call<Child> call = this.service.postChild(sectionName, child);
        call.enqueue(new Callback<Child>() {
            @Override
            public void onResponse(Call<Child> call, Response<Child> response) {
                if(call.isExecuted() && response.code() == RESPONSE_OK_NO_CONTENT) {
                    callback.onPostSuccess();
                }
            }

            @Override
            public void onFailure(Call<Child> call, Throwable t) {
                t.printStackTrace();
                callback.onPostFailure();
            }
        });
    }

    public void deleteChild(String socialSecurityNumber, Child child, OnChildDeletedCallback callback) {
        Call<Child> call = this.service.deleteChild(socialSecurityNumber, child);
        call.enqueue(new Callback<Child>() {
            @Override
            public void onResponse(Call<Child> call, Response<Child> response) {
                if(call.isExecuted() && response.code() == RESPONSE_OK_NO_CONTENT) {
                    callback.onDeleteSuccess();
                }
            }

            @Override
            public void onFailure(Call<Child> call, Throwable t) {
                t.printStackTrace();
                callback.onDeleteFailure();
            }
        });
    }

    public void updateChild(Child child, OnChildUpdatedCallback callback) {
        Call<Child> call = this.service.updateChild(child);
        call.enqueue(new Callback<Child>() {
            @Override
            public void onResponse(Call<Child> call, Response<Child> response) {
                if(call.isExecuted() && response.code() == RESPONSE_OK_NO_CONTENT) {
                    callback.onUpdateSuccess();
                }
            }

            @Override
            public void onFailure(Call<Child> call, Throwable t) {
                t.printStackTrace();
                callback.onUpdateFailure();
            }
        });
    }

    public interface OnChildrenLoadedCallback {
        void onChildrenLoaded(List<Child> children);
        void onChildrenLoadedFailure();
    }

    public interface OnChildLoadedCallback {
        void onChildLoaded(Child child);
        void onChildLoadedFailure();
    }

    public interface OnChildPostedCallback {
        void onPostSuccess();
        void onPostFailure();
    }

    public interface OnChildDeletedCallback {
        void onDeleteSuccess();
        void onDeleteFailure();
    }

    public interface OnChildUpdatedCallback {
        void onUpdateSuccess();
        void onUpdateFailure();
    }
}
