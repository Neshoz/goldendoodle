package com.lgthouse.goldendoodle.Retrofit.Admin;

import com.lgthouse.goldendoodle.POJO.Admin;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Nesho on 2017-05-05.
 */

public interface AdminService {
    @GET("admins")
    Call<List<Admin>> getAllAdmins();

    @GET("admins/{username}")
    Call<Admin> getAdmin(@Path("username") String username);

    @POST("admins")
    Call<Admin> postAdmin(@Body Admin admin);

    @POST("admins/update")
    Call<Admin> updateAdmin(@Body Admin admin);

    @POST("admins/delete")
    Call<Admin> deleteAdmin(@Body Admin admin);
}
