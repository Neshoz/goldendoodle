package com.lgthouse.goldendoodle.Panel;

/**
 * Created by Nesho on 2017-05-08.
 */

public interface PanelMvp {

    interface View {

    }

    interface Presenter{
        void onDestroy();
    }
}
