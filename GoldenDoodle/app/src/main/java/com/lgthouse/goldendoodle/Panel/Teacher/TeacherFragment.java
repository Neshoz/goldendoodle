package com.lgthouse.goldendoodle.Panel.Teacher;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.lgthouse.goldendoodle.POJO.Teacher;
import com.lgthouse.goldendoodle.Panel.Teacher.add.AddTeacherActivity;
import com.lgthouse.goldendoodle.Panel.Teacher.edit.EditTeacherActivity;
import com.lgthouse.goldendoodle.R;

import java.util.List;

/**
 * Created by Nesho on 2017-05-08.
 */

public class TeacherFragment extends Fragment implements TeacherMvp.View, TeacherAdapter.OnEditClickListener, TeacherAdapter.OnDeleteClickListener {

    private TeacherMvp.Presenter presenter;
    private TeacherAdapter teacherAdapter;
    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private FloatingActionButton addButton;
    private Teacher clickedTeacher;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_teacher, container, false);

        this.presenter = new TeacherPresenter(this);
        this.teacherAdapter = new TeacherAdapter(this.getContext(), this, this);

        this.recyclerView = (RecyclerView)view.findViewById(R.id.teacher_list);
        this.addButton = (FloatingActionButton)view.findViewById(R.id.teacher_add_button);
        this.progressBar = (ProgressBar)view.findViewById(R.id.teacher_fragment_loading_spinner);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this.getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(teacherAdapter);

        this.addButton.setOnClickListener(btn -> this.presenter.navigateToAdd());
        this.presenter.loadTeachers();

        return view;
    }

    @Override
    public void setTeachers(List<Teacher> teachers) {
        this.teacherAdapter.setItems(teachers);
    }

    @Override
    public void deleteTeacher(Teacher teacher) {
        this.clickedTeacher = teacher;
        this.presenter.deleteTeacher(teacher);
    }

    @Override
    public void showProgress() {
        this.recyclerView.setVisibility(View.GONE);
        this.progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        this.progressBar.setVisibility(View.GONE);
        this.recyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showConnectionFailed() {
        Snackbar snackbar = Snackbar.make(this.getActivity().findViewById(R.id.activity_panel), getString(R.string.connectionFailed), Snackbar.LENGTH_LONG)
                .setAction(getString(R.string.retryConnection), view -> this.presenter.loadTeachers());
        snackbar.show();
    }

    @Override
    public void showDeleteConnectionFailed() {
        Snackbar snackbar = Snackbar.make(this.getActivity().findViewById(R.id.activity_panel), getString(R.string.connectionFailed), Snackbar.LENGTH_LONG)
                .setAction(getString(R.string.retryConnection), view -> this.presenter.deleteTeacher(this.clickedTeacher));
        snackbar.show();
    }

    @Override
    public void navigateToAdd() {
        this.startActivity(new Intent(this.getContext(), AddTeacherActivity.class));
    }

    @Override
    public void navigateToEdit(Teacher teacher) {
        this.startActivity(EditTeacherActivity.newIntent(this.getContext(), teacher.getSocialSecurityNumber()));
    }

    @Override
    public void onDeleteClicked(Teacher teacher) {
        this.presenter.deleteTeacher(teacher);
    }

    @Override
    public void onEditClicked(Teacher teacher) {
        this.presenter.navigateToEdit(teacher);
    }

    @Override
    public void onDestroy() {
        this.presenter.onDestroy();
        super.onDestroy();
    }
}
