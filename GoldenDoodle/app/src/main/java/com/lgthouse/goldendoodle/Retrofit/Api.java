package com.lgthouse.goldendoodle.Retrofit;

import com.lgthouse.goldendoodle.AppUtil.Constants;
import com.lgthouse.goldendoodle.Retrofit.Admin.AdminService;
import com.lgthouse.goldendoodle.Retrofit.Adult.AdultService;
import com.lgthouse.goldendoodle.Retrofit.Child.ChildService;
import com.lgthouse.goldendoodle.Retrofit.Section.SectionService;
import com.lgthouse.goldendoodle.Retrofit.Teacher.TeacherService;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Nesho on 2017-05-05.
 */

public class Api {
    private static Api instance = null;

    private AdminService adminService;
    private AdultService adultService;
    private ChildService childService;
    private SectionService sectionService;
    private TeacherService teacherService;

    public static Api getInstance() {
        if(instance == null) {
            instance = new Api();
        }

        return instance;
    }

    private Api() {
        this.buildRetrofit();
    }

    private void buildRetrofit() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        this.adminService = retrofit.create(AdminService.class);
        this.adultService = retrofit.create(AdultService.class);
        this.childService = retrofit.create(ChildService.class);
        this.sectionService = retrofit.create(SectionService.class);
        this.teacherService = retrofit.create(TeacherService.class);
    }

    public AdminService getAdminService() {
        return this.adminService;
    }
    public AdultService getAdultService() {
        return this.adultService;
    }
    public ChildService getChildService() {
        return this.childService;
    }
    public SectionService getSectionService() {
        return this.sectionService;
    }
    public TeacherService getTeacherService() {
        return this.teacherService;
    }
}
