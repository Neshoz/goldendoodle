package com.lgthouse.goldendoodle.Panel.Admin;

import com.lgthouse.goldendoodle.POJO.Admin;
import com.lgthouse.goldendoodle.Retrofit.Admin.AdminController;

import java.util.List;

/**
 * Created by Nesho on 2017-05-08.
 */

public class AdminPresenter implements AdminMvp.Presenter {

    private AdminMvp.View view;
    private AdminController adminController;

    public AdminPresenter(AdminMvp.View view) {
        this.view = view;
        this.adminController = new AdminController();
    }

    @Override
    public void loadAdmins() {
        this.view.showProgress();
        this.adminController.getAllAdmins(new AdminController.OnAdminsLoadedCallback() {
            @Override
            public void onAdminsLoaded(List<Admin> admins) {
                view.hideProgress();
                view.setAdmins(admins);
            }

            @Override
            public void onAdminsLoadedFailure() {
                view.hideProgress();
                view.showConnectionFailed();
            }
        });
    }

    @Override
    public void navigateToEdit(Admin admin) {
        this.view.navigateToEdit(admin);
    }

    @Override
    public void navigateToAdd() {
        this.view.navigateToAdd();
    }

    @Override
    public void removeAdmin(Admin admin) {
        this.view.showProgress();
        this.adminController.deleteAdmin(admin, new AdminController.OnAdminDeletedCallback() {
            @Override
            public void onDeleteSuccess() {
                view.hideProgress();
                view.removeAdmin(admin);
            }

            @Override
            public void onDeleteFailure() {
                view.hideProgress();
                view.showDeleteConnectionFailed();
            }
        });
    }

    @Override
    public void onDestroy() {
        this.view = null;
    }
}
