package com.lgthouse.goldendoodle.Panel.Teacher;

import com.lgthouse.goldendoodle.POJO.Teacher;

import java.util.List;

/**
 * Created by Nesho on 2017-05-08.
 */

public interface TeacherMvp {

    interface View {
        void setTeachers(List<Teacher> teachers);
        void deleteTeacher(Teacher teacher);
        void showProgress();
        void hideProgress();
        void showConnectionFailed();
        void showDeleteConnectionFailed();
        void navigateToAdd();
        void navigateToEdit(Teacher teacher);
    }

    interface Presenter {
        void loadTeachers();
        void deleteTeacher(Teacher teacher);
        void navigateToAdd();
        void navigateToEdit(Teacher teacher);
        void onDestroy();
    }
}
