package com.lgthouse.goldendoodle.Panel.Section;

import com.lgthouse.goldendoodle.POJO.Section;

import java.util.List;

/**
 * Created by Nesho on 2017-05-08.
 */

public interface SectionMvp {

    interface View {
        void setSections(List<Section> sections);
        void showProgress();
        void hideProgress();
        void showConnectionFailed();
        void navigateToEdit(Section section);
    }

    interface Presenter {
        void loadSections();
        void navigateToEdit(Section section);
        void onDestroy();
    }
}
