package com.lgthouse.goldendoodle.Panel.Adult.add;

/**
 * Created by Nesho on 2017-05-09.
 */

public interface AddAdultMvp {

    interface View {
        void showPostProgress();
        void hideProgress();
        void showConnectionFailed();
        void navigateToPanel();
        String getFirstName();
        String getLastName();
        String getSocialSecurityNumber();
        String getPhoneNumber();
        String getWorkPhoneNumber();
        String getEmail();
        String getChildSocialSecurityNumber();
        String getRole();
    }

    interface Presenter {
        void postAdult();
        void onDestroy();
    }
}
